<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Policy;

/**
 * PolicySearch represents the model behind the search form about `frontend\models\Policy`.
 */
class PolicySearch extends Policy
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'policy_id', 'sex', 'nationality'], 'integer'],
            [['date_at', 'insurance_type', 'number', 'insurance_company', 'status', 'cost', 'date_end', 'url', 'calculation_id', 'date_paid', 'surname', 'name', 'middle_name', 'date_born', 'phone', 'email', 'document_type', 'document_serie', 'document_number', 'document_date', 'document_by', 'city', 'street', 'house', 'housing', 'apartment'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Policy::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'date_at' => $this->date_at,
            'date_end' => $this->date_end,
            'policy_id' => $this->policy_id,
            'date_paid' => $this->date_paid,
            'sex' => $this->sex,
            'nationality' => $this->nationality,
        ]);

        $query->andFilterWhere(['like', 'insurance_type', $this->insurance_type])
            ->andFilterWhere(['like', 'number', $this->number])
            ->andFilterWhere(['like', 'insurance_company', $this->insurance_company])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'cost', $this->cost])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'calculation_id', $this->calculation_id])
            ->andFilterWhere(['like', 'surname', $this->surname])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'middle_name', $this->middle_name])
            ->andFilterWhere(['like', 'date_born', $this->date_born])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'document_type', $this->document_type])
            ->andFilterWhere(['like', 'document_serie', $this->document_serie])
            ->andFilterWhere(['like', 'document_number', $this->document_number])
            ->andFilterWhere(['like', 'document_date', $this->document_date])
            ->andFilterWhere(['like', 'document_by', $this->document_by])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'street', $this->street])
            ->andFilterWhere(['like', 'house', $this->house])
            ->andFilterWhere(['like', 'housing', $this->housing])
            ->andFilterWhere(['like', 'apartment', $this->apartment]);

        $query->addOrderBy('date_at DESC');

        return $dataProvider;
    }
}
