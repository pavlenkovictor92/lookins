<?php
use yii\helpers\Html;
use frontend\assets\AppAsset;
use yii\widgets\Menu;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="/favicon.png" />
	<link rel="apple-touch-icon-precomposed" href="/apple-touch-favicon.png"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="index">

<?php $this->beginBody() ?>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-60674312-1', 'auto');
  ga('send', 'pageview');
</script>

<div class="wrapper">

	<header class="header">
        <ul>
            <li><img src="/img/logo.png" /></li>
            <?php if(Yii::$app->user->isGuest):?>
            <li>
                <p>LOOKайте нас:</p>
                <ul class="social">
                    <li><a href="http://vk.com/public90313658" target="_blank"><img src="/img/s1.png" /></a></li>
                    <li><a href="https://www.facebook.com/groups/1420765441569692/" target="_blank"><img src="/img/s2.png" /></a></li>
                    <li><a href="http://ok.ru/group/53467852636264" target="_blank"><img src="/img/s3.png" /></a></li>
                    <li><a href="https://instagram.com/lookins.ru/" target="_blank"><img src="/img/s4.png" /></a></li>
                    <li><a href="https://twitter.com/Lookins_ru" target="_blank"><img src="/img/s5.png" /></a></li>
                    <!-- <li><a href="#"><img src="/img/s6.png" /></a></li> -->
                </ul>
            </li>
            <li><?= Html::a('Войти', ['site/login']);?></li>
                <?php else:?>
            <li><?= Html::a('Мои Полисы', ['/cabinet'], ['class' => 'btn btn-default']);?></li>
            <li>
                <span class="dropdown">
                    <a href="#" data-toggle="dropdown" class="dropdown-toggle links">Настройки<b class="caret"></b></a>
                    <?= \yii\bootstrap\Dropdown::widget([
                        'items' => [
                            ['label' => 'Редактирование данных о себе', 'url' => '/cabinet/profile'],
                            ['label' => 'Изменение пароля', 'url' => '/cabinet/change-password']
                        ]
                    ]);?>
                </span>
                <span><?= Html::a('Выйти', ['/site/logout'], ['class' => 'links']);?></span>
            </li>
                <?php endif;?>
        </ul>
    </header><!-- .header-->

	<main class="content">
        <div class="row">
            <div class="col-md-12">
                <?= $content ?>
            </div>
        </div>

	</main><!-- .content -->

</div><!-- .wrapper -->

<footer class="footer">
    <div class="main">
        <ul class="block_footer">
            <li>
                <img src="/img/logo2.png" />
                <p>© 2015 ООО "Капасити". Страхование Онлайн</p>
                <p><em>Все права защищены</em></p>
            </li>
            <li>
		<?php echo Menu::widget([
			'items' => [
				['label' => 'О нас', 'url' => ['/site/page', 'page'=>'about']],
				['label' => 'Партнеры', 'url' => ['/site/page', 'page'=>'partners']],
				['label' => 'Как это работает', 'url' => ['/site/page', 'page'=>'how-this-work']],
				['label' => 'Информация', 'url' => ['/site/page', 'page'=>'information']],
				['label' => 'Контакты', 'url' => ['/site/page', 'page'=>'contacts']],
			],
			'options' => [
				'class' => 'menu_footer'
			]
		]); ?>
                <ul class="two_parts">
                    <li>
                        <img src="/img/visa.png" />
                        <img src="/img/mc.png" />
                    </li>
                    <li>
						<p>LOOKайте нас:<a href="http://vk.com/public90313658" target="_blank" class="soc-common soc1"></a><a href="https://www.facebook.com/groups/1420765441569692/" target="_blank" class="soc-common soc2"></a><a href="http://ok.ru/group/53467852636264" target="_blank" class="soc-common soc3"></a><a href="https://instagram.com/lookins.ru/" target="_blank" class="soc-common soc4"></a><a href="https://twitter.com/Lookins_ru" target="_blank" class="soc-common soc5"></a><!--<a href="#" class="soc-common soc6"></a>--></p>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</footer><!-- .footer -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>