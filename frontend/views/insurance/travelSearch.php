<?php
use frontend\models\VazhnoTravel;
use frontend\assets\MainAsset;
use frontend\assets\ValidationJsAsset;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use yii\helpers\Html;
use yii\jui\AutoComplete;
use yii\helpers\Url;
use yii\jui\SliderInput;
use yii\widgets\ListView;

MainAsset::register($this);
ValidationJsAsset::register($this);

$form = ActiveForm::begin([
    'id'    => 'travel-insurance',
    'enableAjaxValidation'  => false,
    'enableClientValidation'=> false,
]);
?>
<div class="look-travel-search">
    <h2>Страхование выезжающих за рубеж</h2>

    <div class="row">
        <div class="col-xs-8">
            <div class="look-travel-search-form">

                <div class="col-xs-4 first">
                    <?= $form->field($main, 'country')->widget(AutoComplete::classname(), [
                        'options'       => ['id'    => 'country', 'placeholder' => 'Страна поездки', 'value' => \Yii::$app->session->get('travel.country')],
                        'clientOptions' => ['source' => $main->getApiCountries()],
                    ])->label(false); ?>
                </div>
                <div class="col-xs-4 date-block">
                    <div class="date-block-fake">
                        <?= Html::input('text', 'dateFake', explode('.', $main->dateBegin)[0].".".explode('.', $main->dateBegin)[1].' - '.explode('.', $main->dateEnd)[0].".".explode('.', $main->dateEnd)[1] , ['id' => 'dateFake', 'placeholder' => 'Когда?'], false); ?>
                    </div>
                    <div class="date-block-begin">
                        <?= $form->field($main, 'dateBegin')->widget(DatePicker::className(), [
                            'language'  => 'ru',
                            'dateFormat'=> 'dd.MM',
                            'options'       => ['id' => 'dateBegin'],
                            'clientOptions' => [
                                'minDate'       => '+1',
//                                'defaultDate'   => $main->dateBegin,
//                                'setDate'       => $main->dateBegin
                            ]
                        ])->label(false);?>
                    </div>
                    <div class="date-block-end">
                        <?= $form->field($main, 'dateEnd')->widget(DatePicker::className(), [
                            'language'  => 'ru',
                            'dateFormat'=> 'dd.MM',
                            'options'       => ['id' => 'dateEnd'],
                            'clientOptions' => [
                                'minDate'       => '+1',
//                                'defaultDate'   => $main->dateEnd,
//                                'setDate'       => $main->dateEnd
                            ]
                        ])->label(false);?>
                    </div>
                </div>
                <div class="col-xs-4">
                    <?= $form->field($main, "quantityInsurant",
                        ['template' => '{label}<div class="customArrow">{input}</div>{error}{hint}'])->dropDownList(
                        $main->getQuantityInsurant(), [
                            'id'    => 'quantityInsurant',
                            'value' => \Yii::$app->session->get('travel.quantityInsurant')
                    ])->label(false);?>
                </div>

                <div class="first col-xs-4">
                    <?php if(\Yii::$app->session->get('travel.isManyTravel')){
                        $check = 'checked';
                    }else{
                        $check = 'uncheck';
                    }?>
                    <?= Html::activeInput('checkbox', $main, 'isManyTravel', ['id' => 'isManyTravel', 'value' => \Yii::$app->session->get('travel.isManyTravel'), $check => \Yii::$app->session->get('travel.isManyTravel') ? 'checked' : ''], false); ?>
                    <?= Html::activeLabel($main, 'isManyTravel', ['for' => 'isManyTravel']); ?>
                </div>

                <div class="col-xs-4">
                    <div id="quantityDays_parent" class="hide">
                        <?= $form->field($main, "quantityDays",
                            ['template' => '{label}<div class="customArrow">{input}</div>{error}{hint}'])->dropDownList(
                            $main->getQuantityDays(), [
                            'id'    => 'quantityDays',
                            'value' => \Yii::$app->session->get('travel.quantityDays')
                        ]); ?>
                    </div>
                </div>

                <div class="col-xs-4 center">
                    <a href="<?=Url::toRoute(['travel-advanced'])?>" class="btn">Найти страховку</a>
                </div>
            </div>
            <div class="look-travel-search-result">
                <h2>Результаты поиска</h2>
<!--                --><?php //= ListView::widget([
//                    'dataProvider' => $dataProvider,
//                    'itemOptions' => ['class' => 'item'],
//                    'template'=>"{items}",
//                    'itemView' => function ($main, $key, $index, $widget) {
//
////                        $newModel = array_filter($model, function($obj){
////                            if($obj->post_author  == 'vasia')
////                                return true;
////                        });
//
//                        return $this->render('_searchResult', ['main' => $main]);
//                    },
//                ]) ?>
                <div class="look-travel-search-result-block clearfix">
                    <div class="col-xs-8">
                        <div class="row">
                            <div class="col-xs-6">
                                <img src="<?= Yii::getAlias('@web') ?>/img/log6.png" alt="">
                            </div>
                            <div class="col-xs-6">
                                <div class="look-travel-search-result-block-sum">Страховая сумма <br> <b><span class="insurance-sum insuranceAmount-result"><?= $main->insuranceAmount; ?> &euro;</span></b></div>
                            </div>
                            <div class="col-xs-12">
                                <div class="show-advanced-options look-travel-search-result-block-options">
                                    <span>Дополнительные опции страхования</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="look-travel-search-result-block-price">
                            <span id="total">1600, 36</span>
                            <span><del>Р</del></span>
                        </div>
                        <div  id="by" class="btn">Купить страховку</div>
                    </div>
                    <div class="col-xs-12">
                        <div class="advanced-options" style="display: none;">
                            <div class="look-travel-search-result-block-options-advanced clearfix">

                                <div class="col-xs-12 first">
                                    <?= Html::activeInput('checkbox', $main, 'isActiveSport', ['id' => 'isActiveSport'], false); ?>
                                    <?= Html::activeLabel($main, 'isActiveSport', ['for' => 'isActiveSport', 'class' => 'pull-left mainSport']); ?>
                                    <div id="typeSport-container" class="hide pull-left hint relative">
                                        <?= $form->field($main, 'typeSport', ['template' => '{label} <div class="customArrow">{input}</div>'])->dropDownList($main->getTypeSport(), [
                                            'id' => 'typeSport',
                                        ], false)->label(false); ?>
                                    </div>
                                </div>

                                <div class="col-xs-12 first">
                                    <?= $form->field($main, 'risks')->checkboxList($main->getRisks(true), [
                                        'item' =>
                                            function ($index, $label, $name, $checked, $value) {
                                                return Html::checkbox($name, $checked, [
                                                    'value' => $value,
                                                    'label' => '<label for="risk-'.$value.'" class = "hint relative">' . $label . '</label>',
                                                    'id' => 'risk-'.$value
                                                ], false);
                                            },
                                        'id' => 'risks',
                                        'class' => 'all-risks',
                                        'separator' => '<br>'
                                    ], false, false)->label(false); ?>



                                    <div id="goSportAmount-container" class="row hide">
<!--                                        <div class="col-xs-8">-->
<!--                                            --><?//= $form->field($main, 'goSportAmount', ['template' => '{label}<div class="customArrow">{input}</div>{error}{hint}'])->dropDownList($main->getGoSportAmounts(), [
//                                                'id' => 'goSportAmount'
//                                            ]); ?>
<!--                                        </div>-->
<!--                                        <div class="col-xs-4">-->
<!--                                            --><?//= $form->field($main, 'goSportCurrency', ['template' => '{label}<div class="customArrow">{input}</div>{error}{hint}'])->dropDownList($main->getGoSportCurrency(), [
//                                                'id' => 'goSportCurrency'
//                                            ])->label(''); ?>
<!--                                        </div>-->

                                        <div class="goSportAmount">
                                            <div class="goSportAmount-result">10000 &euro;</div>
                                            <?= Html::activeInput('text', $main, 'goSportAmount', ['id' => 'goSportAmount' ]); ?>
                                        </div>
                                    </div>
                                </div>
<!--                                <div>-->
<!--                                    <div class="customArrow">-->
<!--                                        --><?php //= $form->field($main, 'assistanceCompany')->dropDownList($main->getAssistanceCompany(), [
//                                            'id'    => 'assistanceCompany'
//                                        ]);?>
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="row">-->
<!--                                    <div class="col-xs-8">-->
<!--                                        --><?php //= $form->field($main, 'insuranceAmount', ['template' => '{label}<div class="customArrow">{input}</div>{error}{hint}'])->dropDownList($main->getInsuranceAmount(), [
//                                            'id' => 'insuranceAmount'
//                                        ]); ?>
<!--                                    </div>-->
<!--                                    <div class="col-xs-4">-->
<!--                                        --><?php //= $form->field($main, 'insuranceAmountCurrency', ['template' => '{label}<div class="customArrow">{input}</div>{error}{hint}'])->dropDownList($main->getGoSportCurrency(), [
//                                            'id' => 'insuranceAmountCurrency'
//                                        ])->label(''); ?>
<!--                                    </div>-->
<!--                                </div>-->
                                <div class="col-xs-8 first">

                                <div class="insuranceAmount hide">
                                    <div class="insuranceAmount-result"><?= $main->insuranceAmount; ?> &euro;</div>
                                    <?= Html::activeInput('text', $main, 'insuranceAmount', ['id' => 'insuranceAmountMain' ]); ?>
                                </div>
                                </div>
                                <div class="col-xs-12 first">
                                    <?= $form->field($main, 'calculationId')->hiddenInput([
                                        'id' => 'calculationId'
                                    ])->label(false); ?>
                                    <?= $form->field($main, 'calculationAmount')->hiddenInput([
                                        'id' => 'calculationAmount'
                                    ])->label(false); ?>
                                    <?= Html::hiddenInput('type', $main->className(), ['id' => 'type']); ?>
                                    <?= Html::hiddenInput('policyNumber', '', ['id' => 'policyNumber']); ?>
                                </div>
                            </div>


                            <div class="hide-advanced-options look-travel-search-result-block-options-advanced-hide">

                            </div>
                        </div>
                    </div>
                </div>
          </div>
        </div>
        <div class="col-xs-4">
            <div class="look-travel-search-options">
                <div class="look-travel-search-options-block">
                    <h4>Территория страхования</h4>

                    <div class="look-travel-search-options-block-details">
                        <span class="text country"><?= \Yii::$app->session->get('travel.country'); ?></span>
                    </div>
                </div>
                <div class="look-travel-search-options-block">
                    <h4>Возраст путешественников</h4>
                    <div class="look-travel-search-options-block-details">
                        <ul class="updates text clearfix">

                        </ul>
                    </div>
                </div>
                <div class="look-travel-search-options-block">
                    <h4>Базовый вариант</h4>
                    <div class="look-travel-search-options-block-details sport">
                        <span class="text" >Страховая сумма</span>
                        <div class="insuranceAmount">
                        <div class="insuranceAmount-result"><?= $main->insuranceAmount; ?> &euro;</div>
                            <?= Html::activeInput('text', $main, 'insuranceAmount', ['id' => 'insuranceAmount' ]); ?>
                        </div>
<!--                        --><?php //= $form->field($main, 'insuranceAmountSliderOption')->widget(SliderInput::className(), [
//                            'clientOptions' => [
//                                'min'   => 20000,
//                                'max'   => 1000000,
//                                "orientation" => "horizontal",
//                                "range" => "min",
//                                'step'  => 3500,
//                                'value' => 100000
//                            ]
//                        ])->label('');?>
                    </div>
                    <div class="look-travel-search-options-block-details">

                        <div class="relative">
                            <?= Html::checkbox('mainSport', false, ['id' => 'mainSport']); ?>
                            <?= Html::label('Занятия спортом', 'mainSport', ['class' => 'hint ']); ?>
                            <div class="info" style="display: none">
                                Активный отдых требует особой осторожности. Выбирайте опцию, если планируете заниматься любительским спортом.
                            </div>
                        </div>
                    </div>
                </div>
                <div class="look-travel-search-options-block">
                    <h4>Дополнительные опции</h4>
                    <div class="look-travel-search-options-block-details">
                        <div class="relative">
                            <?= Html::checkbox('mainBaggageInsurance', false, ['id' => 'mainBaggageInsurance'], false); ?>
                            <?= Html::label('Страхование багажа', 'mainBaggageInsurance', ['class' => 'hint mainBaggageInsurance']); ?>
                            <div class="info" style="display: none">
                                При потере или повреждении багажа авиаперевозчиком Вы получите компенсацию от страховой.
                            </div>
                        </div>
                        <div class="relative">
                            <?= Html::checkbox('mainTripCancel', false, ['id' => 'mainTripCancel'], false); ?>
                            <?= Html::label('Отмена поездки', 'mainTripCancel', ['class' => 'hint mainTripCancel']); ?>
                            <div class="info" style="display: none">
                                Страховая компания вернет Вам часть стоимости авиабилета, если поездка не может состояться по независящим от Вас причинам.
                            </div>
                        </div>
                        <div class="relative">
                            <?= Html::checkbox('mainCivilResponsibility', false, ['id' => 'mainCivilResponsibility'], false); ?>
                            <?= Html::label('Гражданская ответсвенность', 'mainCivilResponsibility', ['class' => 'hint mainCivilResponsibility']); ?>
                            <div class="info" style="display: none">
                                При нанесении ущерба третьим лицам, страховая компания возмести ущерб за Вас.
                            </div>
                        </div>
                        <div class="relative">
                            <?= Html::checkbox('mainFlightDelay', false, ['id' => 'mainFlightDelay'], false); ?>
                            <?= Html::label('Задержка рейса', 'mainFlightDelay', ['class' => 'hint mainFlightDelay']); ?>
                            <div class="info" style="display: none">
                                Возмещение документально подтверждённых расходов  при задержке рейса.
                            </div>
                        </div>
                        <div class="relative">
                            <?= Html::checkbox('mainAccident', false, ['id' => 'mainAccident'], false); ?>
                            <?= Html::label('Несчастный случай', 'mainAccident', ['class' => 'hint mainAccident']); ?>
                            <div class="info" style="display: none">
                                Выплата на каждого путешественника при получении травмы, установлении инвалидности или смерти во время перелета «туда/обратно»
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?= Html::hiddenInput('countryName', '',['class' => 'countryName' ]); ?>
    <div class="hide">
        <?php $apiCountries = $main->getApiCountriesByGroup();?>
        <?php foreach($apiCountries->countries as $local => $countries):?>
            <div class="part_world hide">
                <a href="#" class="part_world_country"><?= $local;?></a>
                <ul class="double_list hide">
                    <?php foreach($countries as $country):?>
                        <li><a href="#" class="selectCountry"><?=$country->name;?></a></li>
                    <?php endforeach;?>
                </ul>
            </div>
        <?php endforeach;?>
    </div>


    <div class="risks-hint hide">
        <div class="info risk-0" style="display: none">
            При потере или повреждении багажа авиаперевозчиком Вы получите компенсацию 1500 рублей за каждый килограмм утраченного багажа и 750 рублей за килограмм испорченного багажа в пределах 30 000 рублей
        </div>
        <div class="info risk-1" style="display: none">
            Страховая компания вернет Вам 90% стоимости авиабилета в пределах 25 000 рублей, если поездка не может состояться из-за отказа в визе, госпитализации, ДТП по дороге в аэропорт или по другим причинам
        </div>
        <div class="info risk-2" style="display: none">
            Выплата на каждого путешественника при получении травмы, установлении инвалидности или смерти во время перелета «туда/обратно»
        </div>
        <div class="info risk-3" style="display: none">
            Если авиакомпания задержит доставку багажа более чем на 12 часов, Вы сможете приобрести личные вещи на сумму до 10 000 рублей, и расходы будут компенсированы страховой компанией
        </div>
        <div class="info risk-4" style="display: none">
            В случае кражи или порчи багажа в гостинице Вы получите компенсацию в пределах 30 000 рублей
        </div>
        <div class="info risk-5" style="display: none">
            Компенсация до 500 000 рублей при потере имущества в случае кражи/пожара/затопления и Вашей гражданской ответственности перед соседями
        </div>
        <div class="info risk-6" style="display: none">
            При задержке регулярного рейса более чем на 3 часа Вы получите компенсацию 1 000 рублейза каждый полный час задержки
        </div>
        <div class="info risk-7" style="display: none">
            При задержке чартерного рейса более чем на 6 часов Вы получите компенсацию 1 000 рублей за каждый полный час задержки
        </div>
        <div class="info risk-8" style="display: none">
            В случае опоздания на стыковочный рейс Вы получите компенсацию 1 000 рублей за каждый час ожидания следующего рейса
        </div>
        <div class=" risk-9"  style="display: none">

        </div>
    </div>

    <div class="sport-hint hide">
        <div class="info sport-0" style="display: none">
            Горные лыжи/сноуборд (катание по маркированным трассам), водные лыжи, подводное плавание (до 40 м), рафтинг, езда на снегоходе, мотобайке, маунтинбайк, командные виды спорта (волейбол, футбол, баскетбол и т.д.), серфинг, охота, рыбалка, конный спорт.
        </div>
        <div class="info sport-1" style="display: none">
            Участие в любых соревнованиях, скачки, авто и мотогонки, парусные регаты, подводное плавание с использованием дыхательных аппаратов (дайвингом) и/или длительной задержкой дыхания (фри(скин)-дайвинг), клиф и хай-дайвинг, спортивным туризм.
        </div>
        <div class="info sport-2" style="display: none">
            Фрирайд (внетрассовое катание), парашютный, парапланеризм, дельтапланеризм, спуск в пещеры (спелеология), скалолазание, альпинизм.
        </div>
    </div>
</div>


<?php ActiveForm::end() ?>
<?php $this->registerJsFile('@web/js/vazhno-travel-insurance.js', ['depends' => [\yii\web\JqueryAsset::className(), MainAsset::className()]]);?>

<?php //$this->registerJsFile('@web/js/registration.js', ['depends' => [\yii\web\JqueryAsset::className()]]);?>


<script id="template" type="text/x-handlebars-template">
    {{#each quantity}}
    <div class="col-xs-5">
        <div class="form-group field-vazhnotravel-years_{{number}}">
            <div class="customArrow">
                <select class="form-control valid years{{number}}" value="23" name="VazhnoTravel[years_{{number}}]" aria-invalid="false">
                    {{#each ../years}}
                        <option {{selected this n}} value="{{n}}" >{{n}}</option>
                    {{/each}}
                </select>
            </div>
        </div>
    </div>
    {{/each}}
</script>

<script>

    $(document).ready(function(){

        var risks = <?= json_encode($main->getRisks(false)); ?>;


        $('#by').on('click', function(){
            if($(this).attr('disabled') === undefined){

                var itog = [];

                if($('#isActiveSport').prop('checked')){
                    itog.push($('#typeSport').val().replace(',', ''))
                }

                $('.advanced-options').find('input[type = checkbox]:checked').not('#isActiveSport').each(function(i, v){
                    var risks = <?= json_encode($main->getRisks(false)); ?>;
                    itog.push(risks[$(this).val()])
                });

                if($('#risk-9').prop('checked')){
                    itog.push($('#goSportAmount').val())
                }

                $('<input type="hidden" name="options" value="' + itog +'">').appendTo($('#travel-insurance'));

                $('#travel-insurance').submit();
            }
        });


        Handlebars.registerHelper('selected', function(option, value){
            if (option.n == 23) {
                return ' selected';
            } else {
                return ''
            }
        });

        var data = {
            quantity: [
                {
                    number: 1
                }
            ],
            years: [

            ]
        };

        for(var i = 0; i <= 85; i++){
            data.years.push({n: i})
        }

        var q = $('#quantityInsurant').val();
        data.quantity = [];
        for(var i = 1; i <= q; i++){
            data.quantity.push({number: i})
        }
        var template = Handlebars.compile( $('#template').html() );
        $('.updates').append( template(data) );

        $('#quantityInsurant').on('change', function(){
            $('.updates').empty();
            data.quantity = [];
            var q = $(this).val();

            for(var i = 1; i <= q; i++){
                data.quantity.push({number: i})
            }

            var template = Handlebars.compile( $('#template').html() );
            $('.updates').append( template(data) );
            initRequest()
        });


        var initRequest = function(){
            if(!$('input[name=type]').length > 0){
                return;
            }
            $('#total').html('Загрузка ');
            var dateBegin = $('#dateBegin').val();
            //$('#dateBegin').val($('#dateBegin').val().split('-')[0]);

            function request(){
                $.ajax({
                    type: 'POST',
                    url: window.location.origin + '/insurance/calculate',
                    data: $('#travel-insurance').serialize()
                })
                    .done(function(response){
                        var json = $.parseJSON(response);
                        if(json.error){
                            $('#total').html('---');
                            $('#by').attr('disabled', true)
                        }else{
                            $("input[id=calculationId]").val(json.calculation.calculationId);
                            $("input[id=calculationAmount]").val(json.calculation.premium);
                            $('#total').html(json.calculation.premium + '.00');
                            $('#by').attr('disabled', false)
                        }
                    })
                    .fail(function(response){
                        $("input[id=calculationId]").val('');
                        $("input[id=calculationAmount]").val('');
                        $('#total').html('---')
                        $('#by').attr('disabled', true)
                    }).always(function(){ });
            }
            setTimeout(request, 1000);
        };

        setTimeout(function(){
            $("#dateBegin").datepicker("setDate", '<?= $main->dateBegin; ?>');
            $("#dateEnd").datepicker("setDate", '<?= $main->dateEnd; ?>');
            initRequest();
        }, 1000)
    })
</script>
