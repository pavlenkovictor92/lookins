<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user frontend\models\User */

?>
<div class="password-reset">
    <p>Здравствуйте <?= Html::encode($user->surname.' '.$user->name) ?>.</p>

    <p>Вы успешно зарегистрировались в сервисе Lookins.ru.</p>

    <p>Ваш логин: <?= Html::encode($user->email);?></p>
    <p>Ваш пароль: <?=$password;?></p>
</div>
