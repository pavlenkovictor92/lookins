<?php

namespace frontend\models;
use Yii;
use frontend\models\InsuranceModelAbstract;
use frontend\models\Policy;
use yii\helpers\ArrayHelper;
use yii\db\Expression;
use yii\helpers\Url;
class VazhnoTravel extends InsuranceModelAbstract
{
    protected $type = 'Страхование выезжающих за рубеж';
    protected $company = 'Важно';
    protected $class = __CLASS__;
    protected $code  = 'vazhno_vzr';
    protected $folder = 'vazhno/vzr/';

    public $country                 = 'ГЕРМАНИЯ';
    public $dateBegin;
    public $dateEnd;

    public $quantityInsurant        = 1;
    public $isManyTravel            = 0;
    public $quantityDays            = 1;
    public $insuranceAmount         = 30000;
    public $insuranceAmountSlider;
    public $insuranceAmountSliderOption;
    public $insuranceAmountCurrency = 'EUR';
    public $assistanceCompany       = 'GVA';
    public $isActiveSport           = 0;
    public $typeSport;
    public $risks;
    public $goSportAmount           = 10000;
    public $goSportCurrency         = 'EUR';
    public $calculationId;
    public $calculationAmount;
    public $policyId;
    public $policyNumber;

//    public $mainBaggageInsurance;
//    public $mainTripCancel;
//    public $mainCivilResponsibility;
//    public $mainFlightDelay;
//    public $mainAccident;
//    public $mainSport;
    //dynamic fields

    public $years_1;
    public $years_2;
    public $years_3;
    public $years_4;
    public $years_5;
    public $surname_1;
    public $surname_2;
    public $surname_3;
    public $surname_4;
    public $surname_5;
    public $name_1;
    public $name_2;
    public $name_3;
    public $name_4;
    public $name_5;
    public $birthdate_1;
    public $birthdate_2             = -1;
    public $birthdate_3             = -1;
    public $birthdate_4             = -1;
    public $birthdate_5             = -1;


    public function rules()
    {
        return [
          [['country', 'dateBegin', 'dateEnd', 'insuranceAmount'], 'required'],
          [[  'isManyTravel', 'quantityDays', 'insuranceAmountCurrency',
              'assistanceCompany', 'insuranceAmountSlider', 'insuranceAmountSliderOption',
              'isActiveSport', 'typeSport', 'risks', 'goSportAmount', 'goSportCurrency','calculationId',
              'calculationAmount', 'policyNumber', 'policyId',
              'surname_1','surname_2','surname_3','surname_4','surname_5',
              'name_1','name_2','name_3','name_4','name_5',
              'years_1','years_2','years_3','years_4','years_5',
              'birthdate_2', 'birthdate_3','birthdate_4','birthdate_5','birthdate_1',
          ], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'country'           => 'Страна',
            'dateBegin'         => 'Сроки поездки с ',
            'dateEnd'           => 'по ',
            'isManyTravel'      => 'Несколько поездок',
            'quantityDays'      => 'Количество дней',
            'insuranceAmount'   => 'Страховая сумма',
            'quantityInsurant'  => 'Число застрахованных',
            'assistanceCompany' => 'Ассистанс-компания',
            'isActiveSport'     => 'Активный спорт',
            'typeSport'         => 'Вид спорта',
            'goSportAmount'     => 'Выберите страховую сумму для данного риска:',
        ];
    }

    public function calculate()
    {
        $this->token = $this->authorize($this->getToken());
        $response = $this->requestApi($this->url,[
            'method'    => 'calculation',
            'procedure' => 'vzr.traveler',
            'token'     => $this->token,
            'data'      => $this->prepareCalculationRequestData()
        ]);
        //$response = $this->prepareCalculationRequestData();
        return $response;
    }

    public function process($user)
    {
        $this->token = $this->authorize($this->getToken());

        $request = $this->requestApi($this->url, [
            'method'    => 'policy',
            'procedure' => 'vzr.traveler.add',
            'token'     => $this->token,
            'data'      => $this->prepareDataForAddPolicy($user)
        ]);

        if(isset($request) && !$request->error){
            $this->policyId     = $request->policyId;
            $this->policyNumber = $request->policyNumber;
        }

        return $request;
    }

    public function buy($policy, $card)
    {
        $this->token = $this->authorize($this->getToken());

        $response = $this->requestApi($this->url,[
            'method'    => 'service',
            'procedure' => 'pay.alfa',
            'data'      => [
                'action'  => 'card_req',
                'policyName' => $policy,
                'cardNum'   => $card->number,
                'cardHolder'=> $card->owner,
                'cardYear'  => $card->year,
                'cardMonth' => $card->month,
                'cardCvc'   => $card->cvc,
                'back_url'  => Url::toRoute(['cabinet/buy', 'policyNumber' => $policy], true),
            ]
        ]);
        return $response;
    }

    public function order($orderId)
    {
        $this->token = $this->authorize($this->getToken());

        $request    = $this->requestApi($this->url,[
            'method'    => 'service',
            'procedure' => 'pay.alfa',
            'data'      => [
                'action'    => 'card_resp',
                'orderId'   => $orderId
            ]
        ]);

        return $request;
    }

    public function getCountries()
    {
        $countries = [
            'АБХАЗИЯ', 'АВСТРИЯ', 'АЛАНДСКИЕ ОСТРОВА', 'АЛБАНИЯ', 'АЛЖИР', 'АНГЛИЯ', 'АНГОЛА',
            'АНДОРРА', 'БАНГЛАДЕШ', 'БАХРЕЙН', 'БЕЛЬГИЯ', 'БЕНИН', 'БОЛГАРИЯ', 'БОСНИЯ И ГЕРЦЕГОВИНА',
            'БОТСВАНА', 'БРУНЕЙ - ДАРУССАЛАМ', 'БУРКИНА - ФАСО', 'БУРУНДИ', 'ВЕНГРИЯ', 'ВЬЕТНАМ',
            'ГАБОН', 'ГАМБИЯ', 'ГАНА', 'ГВИНЕЯ', 'ГВИНЕЯ - БИСАУ', 'ГЕРМАНИЯ', 'ГРЕЦИЯ', 'ГРУЗИЯ',
            'ДАНИЯ', 'ДЖИБУТИ', 'ЕГИПЕТ', 'ЗАМБИЯ', 'ЗИМБАБВЕ', 'ИЗРАИЛЬ', 'ИНДИЯ', 'ИНДОНЕЗИЯ',
            'ИОРДАНИЯ', 'ИРАК', 'ИРАН, ИСЛАМСКАЯ РЕСПУБЛИКА', 'ИРЛАНДИЯ', 'ИСЛАНДИЯ', 'ИСПАНИЯ',
            'ИТАЛИЯ', 'КАБО - ВЕРДЕ', 'КАМБОДЖА', 'КАМЕРУН', 'КАТАР', 'КЕНИЯ', 'КИПР', 'КИТАЙ',
            'КОРЕЯ, НАРОДНО - ДЕМОКРАТИЧЕСКАЯ РЕСПУБЛИКА', 'КОРЕЯ, РЕСПУБЛИКА', 'КОТ Д\'ИВУАР',
            'КУВЕЙТ', 'ЛАОССКАЯ НАРОДНО - ДЕМОКРАТИЧЕСКАЯ РЕСПУБЛИКА', 'ЛАТВИЯ', 'ЛЕСОТО', 'ЛИБЕРИЯ',
            'ЛИВАН', 'ЛИТВА', 'ЛИХТЕНШТЕЙН', 'ЛЮКСЕМБУРГ', 'МАВРИКИЙ', 'МАВРИТАНИЯ', 'МАДАГАСКАР',
            'МАКЕДОНИЯ, БЫВШАЯ ЮГОСЛАВСКАЯ РЕСПУБЛИКА', 'МАЛАВИ', 'МАЛАЙЗИЯ', 'МАЛИ', 'МАЛЬДИВЫ',
            'МАЛЬТА', 'МАРОККО', 'МОЗАМБИК', 'МОНАКО', 'МОНГОЛИЯ', 'МЬЯНМА', 'НАМИБИЯ', 'НЕПАЛ',
            'НИГЕР', 'НИГЕРИЯ', 'НИДЕРЛАНДЫ', 'НОРВЕГИЯ', 'ОБЪЕДИНЕННЫЕ АРАБСКИЕ ЭМИРАТЫ', 'ОМАН',
            'ПАКИСТАН', 'ПОЛЬША', 'ПОРТУГАЛИЯ', 'РУАНДА', 'РУМЫНИЯ', 'САУДОВСКАЯ АРАВИЯ', 'СЕЙШЕЛЫ',
            'СЕНЕГАЛ', 'СЕРБИЯ', 'СИНГАПУР', 'СЛОВАКИЯ', 'СЛОВЕНИЯ', 'СУДАН', 'ТАИЛАНД', 'ТАЙВАНЬ (КИТАЙ)',
            'ТАНЗАНИЯ, ОБЪЕДИНЕННАЯ РЕСПУБЛИКА', 'ТОГО', 'ТУНИС', 'ТУРЦИЯ', 'УГАНДА', 'ФИЛИППИНЫ', 'ФИНЛЯНДИЯ',
            'ФРАНЦИЯ', 'ХОРВАТИЯ', 'ЦЕНТРАЛЬНО - АФРИКАНСКАЯ РЕСПУБЛИКА', 'ЧАД', 'ЧЕРНОГОРИЯ', 'ЧЕШСКАЯ РЕСПУБЛИКА',
            'ШВЕЙЦАРИЯ', 'ШВЕЦИЯ', 'ШРИ - ЛАНКА', 'ЭКВАТОРИАЛЬНАЯ ГВИНЕЯ', 'ЭСТОНИЯ', 'ЭФИОПИЯ', 'ЮЖНАЯ АФРИКА',
            'ЮЖНАЯ ОСЕТИЯ', 'ЯПОНИЯ'];
        return $countries;
    }

    public function getTypesSport()
    {
        $typesSport = ['Активный отдых', 'Профессиональный, любительский', 'Экстремальный спорт'];
        return array_combine($typesSport, $typesSport);
    }

    public function getQuantityInsurant()
    {
        $count = [1, 2, 3, 4, 5];
        return array_combine($count, $count);
    }

    public function getQuantityDays()
    {
        $count = [30, 60, 90, 180];
        return array_combine($count, $count);
    }

    public function getYears()
    {
        $count = [];

        for($i = 0; $i < 86; ++$i){
            $count[$i] = $i;
        }

        return array_combine($count, $count);
    }

    public function getAssistanceCompany()
    {
        $companies = ['GVA', 'Mondial Assistance'];
        return array_combine($companies, $companies);
    }

    public function getTypeSport()
    {
        $types = ['Активный отдых', 'Профессиональный, любительский спорт', 'Экстремальный спорт'];
        return  array_combine($types, $types);
    }

    public function getRisks($isPrice = false)
    {
        return $this->getApiRisks($isPrice);
    }

    public function getRiskByIndex($index)
    {
        $risks = $this->getRisks(false);
        return $risks[$index];
    }

    public function getInsuranceAmount()
    {
        $amount = [15000, 30000, 50000, 100000];
        return array_combine($amount, $amount);
    }

    public function getGoSportAmounts()
    {
        $amount = [10000, 30000, 50000];
        return array_combine($amount, $amount);
    }

    public function getGoSportCurrency()
    {
        $currencies = ['EUR', 'USD'];
        return array_combine($currencies, $currencies);
    }

    /*
     * API METHODS. TODO: Extract in class
     */
    protected function requestApi($url, array $params)
    {
        try{
            $client = Yii::$app->guzzle;
            $response = $client->post($url, [
                'verify'    => false,
                'body'    => $params
            ]);
        }catch (\Exception $e){
            throw $e;
        }
        return json_decode($response->getBody());
    }

    protected function getToken()
    {
        $this->url  = Yii::$app->params['vazhno.me']['url'];
        $request    = $this->requestApi($this->url,[
            'method'    => 'token',
            'procedure' => 'get'
        ]);

        return $request->token;
    }

    protected function authorize($token)
    {
        $request = $this->requestApi($this->url, [
            'method'    => 'authorization',
            'procedure' => 'authorize.b2b',
            'token'     => $token,
            'data'      => [
                'email'     => Yii::$app->params['vazhno.me']['email'],
                'password'  => Yii::$app->params['vazhno.me']['password']
            ]
        ]);
        return $request->token;
    }

    public function getApiCountries()
    {
        $result = [];

        $this->token = $this->authorize($this->getToken());

        $request    = $this->requestApi($this->url,[
            'method'    => 'dictionary',
            'procedure' => 'countries',
            'token'      => $this->token
        ]);


        if(!$request->error){
            $result = array_values(array_unique(ArrayHelper::toArray($request->flatlist)));
            $res = array_map('ucfirst', $result);
        }
        $res = array_map('ucfirst', $result);
        return $res;
    }

    public function getApiCountriesByGroup()
    {
        $result = [];

        $this->token = $this->authorize($this->getToken());

        $request    = $this->requestApi($this->url,[
            'method'    => 'dictionary',
            'procedure' => 'countries',
            'token'      => $this->token
        ]);

        if(!$request->error){
            $result = $request;
        }
        return $result;
    }

    public function getApiRisks($isPrice = false)
    {
        $result = [];
        $this->token = $this->authorize($this->getToken());

        $request    = $this->requestApi($this->url,[
            'method'    => 'dictionary',
            'procedure' => 'risks.vzr.traveler',
            'token'      => $this->token
        ]);

        if(!$request->error){
            foreach($request->risks as $risk){
                $name = $risk->name;

                if($isPrice){
                    if($risk->price > 0){
                        $name.= ' '.$risk->price.' - Р.';
                    }
                }
                $result[] = $name;
            }
        }
        return $result;
    }

    protected function prepareCalculationRequestData()
    {
        $data = [];
        $data['country']            = $this->country;
        $data['cost']               = $this->insuranceAmount;
        $data['startInsurance']     = $this->dateBegin;
        $data['endInsurance']       = $this->dateEnd;
        $data['company_assistance'] = $this->assistanceCompany;
        $data['currency']           = $this->insuranceAmountCurrency;

        if($this->isManyTravel > 0){
            $data['manyTrips']  = 1;
            $data['days']       = $this->quantityDays;
        }

        if($this->isActiveSport > 0){
            $data['sport'] = $this->typeSport;
        }

        if($this->risks){
            $risks = $this->prepareRisksForApi($this->risks);
            $data['risks'] = $risks;
        }

        if($this->risks && in_array('9', $this->risks)){
            $data['go-cost']    = $this->goSportAmount;
            $data['go-currency']= $this->goSportCurrency;
        }

        if($this->quantityInsurant > 1){
            for($i = 2 ; $i <= $this->quantityInsurant ; $i++){
                $this['birthdate_'.$i] = '11.11.'.(2015 - $this['years_'.$i]);
            }

        }

        $data['insured'][0]['firstname']  = ($this->name_1) ? $this->name_1 : 'Igor';
        $data['insured'][0]['lastname'] = ($this->surname_1)   ? $this->surname_1 : 'Igrov';
        $data['insured'][0]['birthdate'] = '11.11.'.(2015 - $this->years_1);


        if($this->birthdate_2 != -1){
            $data['insured'][1]['firstname'] = ($this->name_2) ? $this->name_2 : 'Petr';
            $data['insured'][1]['lastname']  = ($this->surname_2)   ? $this->surname_2 : 'Petrov';
            $data['insured'][1]['birthdate'] = $this->birthdate_2;
        }
        if($this->birthdate_3 != -1){
            $data['insured'][2]['firstname'] = ($this->name_3) ? $this->name_3 : 'Sasha';
            $data['insured'][2]['lastname']  = ($this->surname_3)   ? $this->surname_3 : 'Sashov';
            $data['insured'][2]['birthdate'] = $this->birthdate_3;
        }
        if($this->birthdate_4 != -1){
            $data['insured'][3]['firstname'] = ($this->name_4) ? $this->name_4 : 'Petr';
            $data['insured'][3]['lastname']  = ($this->surname_4)   ? $this->surname_4 : 'Petrov';
            $data['insured'][3]['birthdate'] = $this->birthdate_4;
        }
        if($this->birthdate_5 != -1){
            $data['insured'][4]['firstname'] = ($this->name_5) ? $this->name_5 : 'Petr';
            $data['insured'][4]['lastname']  = ($this->surname_5)   ? $this->surname_5 : 'Petrov';
            $data['insured'][4]['birthdate'] = $this->birthdate_5;
        }

        return $data;
    }

    protected function prepareDataForAddPolicy($user)
    {
        $preparedData = [
            'calculationId'     => $this->calculationId,
            'country'           => $this->country,
            'cost'              => $this->insuranceAmount,
            'startInsurance'    => $this->dateBegin,
            'endInsurance'      => $this->dateEnd,
            'company_assistance'=> $this->assistanceCompany,
            'insurant'      => [
                'sex'           => $this->getNormalizeGender($user->sex),
                'lastname'      => $user->surname,
                'firstname'     => $user->name,
                'middlename'    => $user->middle_name,
                'birthdate'     => $this->prepareDate($user->date_born),
                'phone'         => $user->phone,
                'email'         => $user->email,
                'documentType'  => Document::getTypeByIndex($user->document_type),
                'documentSerie' => $user->document_serie,
                'documentNumber'=> $user->document_number,
                'documentDate'  => $this->prepareDate($user->document_date),
                'documentIssued'=> $user->document_by,
                'inn'           => ($user->inn) ? $user->inn : '',
                'citizenship'   => Country::getCountryByIndex($user->nationality),
                'country'       => ($user->place_born) ? $user->place_born : '',
                'city'          => $user->city,
                'street'        => $user->street,
                'house'         => $user->house,
                'corpus'        => ($user->housing) ? $user->housing : '',
                'flat'          => ($user->apartment) ? $user->apartment : ''
            ],
            'insured'   => [
                0 => [
                    'firstname' => $this->name_1,
                    'lastname'  => $this->surname_1,
                    'birthdate' => $this->birthdate_1
                ]
            ]
        ];

        if($this->isManyTravel > 0){
            $preparedData['manyTrips']  = 1;
            $preparedData['days']       = $this->quantityDays;
        }

        if($this->birthdate_2 != -1 && $this->name_2 && $this->surname_2){
            $preparedData['insured'][1] = [
                'firstname' => $this->name_2,
                'lastname'  => $this->surname_2,
                'birthdate' => $this->birthdate_2
            ];
        }

        if($this->birthdate_3 != -1 && $this->name_3 && $this->surname_3){
            $preparedData['insured'][2] = [
                'firstname' => $this->name_3,
                'lastname'  => $this->surname_3,
                'birthdate' => $this->birthdate_3
            ];
        }

        if($this->birthdate_4 != -1 && $this->name_4 && $this->surname_4){
            $preparedData['insured'][3] = [
                'firstname' => $this->name_4,
                'lastname'  => $this->surname_4,
                'birthdate' => $this->birthdate_4
            ];
        }

        if($this->birthdate_5 != -1 && $this->name_5 && $this->surname_5){
            $preparedData['insured'][4] = [
                'firstname' => $this->name_5,
                'lastname'  => $this->surname_5,
                'birthdate' => $this->birthdate_5
            ];
        }
        return $preparedData;
    }

    private function getNormalizeGender($index)
    {
        $gender = [
            0   => 'Муж',
            1   => 'Жен'
        ];
        return $gender[$index];
    }

    private function prepareDate($date)
    {
        $strDate    = str_replace('.', '-', $date, $count = 3);
        if(strpos($strDate, '-') !== false){
            $parts      = explode('-', $strDate);
            return $parts[2].'.'.$parts[1].'.'.$parts[0];
        }else{
            return $date;
        }
    }

    protected function prepareRisksForApi($risks)
    {
        $riskFormat = [];
        $i = 1;
        foreach($this->risks as $indexRisk){

            $riskFormat['risk'.$i]['risk'] = $this->getRiskByIndex($indexRisk);
            $i++;
        }
        return $riskFormat;
    }

    public function savePolicyInDB(User $user, $outPolicyData)
    {

        $policyDb                   = new Policy();
        $policyDb->user_id          = $user->id;
        $policyDb->date_at          = new Expression('NOW()');
        $policyDb->insurance_type   = $this->getInsuranceType();
        $policyDb->number           = $outPolicyData->policyNumber;
        $policyDb->insurance_company = $this->getInsuranceCompany();
        $policyDb->status           = Policy::STATUS_WAIT;
        $policyDb->cost             = $this->insuranceAmount;
        $policyDb->calculation_cost = $this->calculationAmount;
        $policyDb->date_start       = $this->prepareDate($this->dateBegin);
        $policyDb->date_end         =  $this->prepareDate($this->dateEnd);
        $policyDb->calculation_id   = $this->calculationId;
        $policyDb->policy_id        = $outPolicyData->policyId;
        $policyDb->class            = $this->class;

        if($user->coincides < 1){
            $policyDb->sex          = $user->sex;
            $policyDb->surname      = $user->surname;
            $policyDb->name         = $user->name;
            $policyDb->middle_name         = $user->middle_name;
            $policyDb->phone         = $user->phone;
            $policyDb->email         = $user->email;
            $policyDb->date_born         = $user->date_born;
            $policyDb->document_type         = $user->document_type;
            $policyDb->document_serie         = $user->document_serie;
            $policyDb->document_number         = $user->document_number;
            $policyDb->document_date         = $user->document_date;
            $policyDb->document_by         = $user->document_by;
            $policyDb->nationality         = $user->nationality;
            $policyDb->city         = $user->city;
            $policyDb->street         = $user->street;
            $policyDb->house         = $user->house;
            $policyDb->housing         = $user->housing;
            $policyDb->apartment         = $user->apartment;
        }else{
            $policyDb->sex          = $user->beneficiary_sex;
            $policyDb->surname      = $user->beneficiary_surname;
            $policyDb->name         = $user->beneficiary_name;
            $policyDb->middle_name         = $user->beneficiary_middle_name;
            $policyDb->phone         = $user->beneficiary_phone;
            $policyDb->email         = $user->beneficiary_email;
            $policyDb->date_born         = $user->beneficiary_date_born;
            $policyDb->document_type         = $user->beneficiary_document_type;
            $policyDb->document_serie         = $user->beneficiary_document_serie;
            $policyDb->document_number         = $user->beneficiary_document_number;
            $policyDb->document_date         = $user->beneficiary_document_date;
            $policyDb->document_by         = $user->beneficiary_document_by;
            $policyDb->nationality         = $user->beneficiary_nationality;
            $policyDb->city         = $user->beneficiary_city;
            $policyDb->street         = $user->beneficiary_street;
            $policyDb->house         = $user->beneficiary_house;
            $policyDb->housing         = $user->beneficiary_housing;
            $policyDb->apartment         = $user->beneficiary_apartment;
        }

        $pdf = $this->pdf($this->policyId);
        if(isset($pdf) && !$pdf->error){
            $filename = $this->folder.$pdf->filename;
            Yii::$app->fs->write($filename, base64_decode($pdf->data));
            $policyDb->url_pdf = $filename;
        }

        if($policyDb->save()){
            return $policyDb;
        }else{
            VarDumper::dump($policyDb, 10, true);
        }
    }

    public function pdf($policyId)
    {
        $request = $this->requestApi($this->url, [
            'method'    => 'policy',
            'procedure' => 'policy.pdf',
            'token'     => $this->token,
            'data'      => [
                'policyId'  => $policyId
            ]
        ]);

        return $request;
    }
}