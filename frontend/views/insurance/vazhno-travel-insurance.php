<?php
use frontend\models\VazhnoTravel;
use frontend\assets\MainAsset;
use frontend\assets\ValidationJsAsset;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use yii\helpers\Html;
use yii\jui\AutoComplete;

MainAsset::register($this);
ValidationJsAsset::register($this);

$form = ActiveForm::begin([
    'id'    => 'travel-insurance',
    'enableAjaxValidation'  => false,
    'enableClientValidation'=> false,
]);
?>


<?php ActiveForm::end() ?>
<?php $this->registerJsFile('@web/js/vazhno-travel-insurance.js', ['depends' => [\yii\web\JqueryAsset::className(), MainAsset::className()]]);?>

<!--<div>-->
<!--    --><?php //$apiCountries = $model->getApiCountriesByGroup();?>
<!--    --><?php //foreach($apiCountries->countries as $local => $countries):?>
<!--        <div class="part_world hide">-->
<!--            <a href="#" class="part_world_country">--><?//= $local;?><!--</a>-->
<!--            <ul class="double_list hide">-->
<!--                --><?php //foreach($countries as $country):?>
<!--                    <li><a href="#" class="selectCountry">--><?//=$country->name;?><!--</a></li>-->
<!--                --><?php //endforeach;?>
<!--            </ul>-->
<!--        </div>-->
<!--    --><?php //endforeach;?>
<!--</div>-->
