<?php

namespace frontend\models;

class Factory {
    public static function createInstance($type)
    {
        return new $type;
    }
}