<?php

namespace frontend\controllers;

use frontend\models\BankCard;
use frontend\models\Policy;
use frontend\models\VazhnoTravel;
use Yii;
use yii\base\Model;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use frontend\models\LiveInsuranceForm;
use frontend\models\User;
use frontend\models\InsuranceProvider;
use frontend\models\Factory;
use yii\data\ActiveDataProvider;

class InsuranceController extends Controller
{
    public $layout = 'main';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /*
     * Форма страхования от несчастных случаев
     */
    public function actionLive()
    {

        if (!Yii::$app->user->isGuest) {
            $user = Yii::$app->user->identity;
        } else {
            $user = new User();
        }

        $liveInsurance = new LiveInsuranceForm();

        return $this->render('live-insurance', [
            'model' => $liveInsurance,
            'registrationForm' => $user,
            'card' => new BankCard(),
        ]);
    }


    /*
     * Форма страхования ВЗР Важно
     */
    public function actionTravel(){

        if (!Yii::$app->user->isGuest) {
            $user = Yii::$app->user->identity;
        } else {
            $user = new User();
        }

        $travelInsurance = new VazhnoTravel();

        if ($travelInsurance->load(Yii::$app->request->post())) {

            $travelInsurance->quantityInsurant = $_POST['VazhnoTravel']['quantityInsurant'];
            \Yii::$app->session->set('travel.dateBegin',        $travelInsurance->dateBegin);
            \Yii::$app->session->set('travel.dateEnd',          $travelInsurance->dateEnd);
            \Yii::$app->session->set('travel.country',          $travelInsurance->country);
            \Yii::$app->session->set('travel.isManyTravel',     $travelInsurance->isManyTravel);
            \Yii::$app->session->set('travel.quantityInsurant', $travelInsurance->quantityInsurant);
//            \Yii::$app->session->set('travel.dateFake',         $travelInsurance->dateFake);

            return $this->redirect(\Yii::$app->urlManager->createUrl("insurance/travel-search"));

        } else {
            return $this->render('travelIndex', [
                'model' => $travelInsurance,
                'user'  => $user,
                'card'  => new BankCard()
            ]);
        }

    }

    public function actionTravelSearch(){
        if (!Yii::$app->user->isGuest) {
            $user = Yii::$app->user->identity;
        } else {
            $user = new User();
        }


        $travelInsurance = new VazhnoTravel();

        if ($travelInsurance->load(Yii::$app->request->post())) {

            \Yii::$app->session->set('travel.calculationId',        $travelInsurance->calculationId);
            \Yii::$app->session->set('travel.calculationAmount',    $travelInsurance->calculationAmount);
            \Yii::$app->session->set('travel.type', Yii::$app->request->post('type'));

            \Yii::$app->session->set('travel.company',          $travelInsurance->quantityInsurant);
            \Yii::$app->session->set('travel.countryName',          $_POST['countryName']);
            \Yii::$app->session->set('travel.dateBegin',        $travelInsurance->dateBegin);
            \Yii::$app->session->set('travel.dateEnd',          $travelInsurance->dateEnd);
            \Yii::$app->session->set('travel.quantityInsurant', $_POST['VazhnoTravel']['quantityInsurant']);
            \Yii::$app->session->set('travel.insuranceAmount',  $travelInsurance->insuranceAmount);
            \Yii::$app->session->set('travel.options',  $_POST['options']);



            return $this->redirect(\Yii::$app->urlManager->createUrl("insurance/travel-advanced"));
        } else {

            $travelInsurance->dateBegin = \Yii::$app->session->get('travel.dateBegin');
            $travelInsurance->dateEnd = \Yii::$app->session->get('travel.dateEnd');
            $travelInsurance->quantityInsurant = \Yii::$app->session->get('travel.quantityInsurant');
            $travelInsurance->isManyTravel = \Yii::$app->session->get('travel.isManyTravel');

            return $this->render('travelSearch', [
                'main' => $travelInsurance,
                'user'  => $user,
//            'dataProvider'  => $dataProvider,
                'card'  => new BankCard()
            ]);
        }

    }

    public function actionTravelAdvanced(){
        if (!Yii::$app->user->isGuest) {
            $user = Yii::$app->user->identity;
        } else {
            $user = new User();
        }

        $travelInsurance = new VazhnoTravel();

        if ($travelInsurance->load(Yii::$app->request->post())) {

            \Yii::$app->session->set('travel.policyNumber', $travelInsurance->policyNumber);

            return $this->redirect(\Yii::$app->urlManager->createUrl("insurance/travel-payment"));
        } else {
            return $this->render('travelAdvanced', [
                'main' => $travelInsurance,
                'user'  => $user,
                'registrationForm' => $user,
//            'dataProvider'  => $dataProvider,
//                'card'  => new BankCard()
            ]);
        }
    }

    public function actionTravelPayment(){
        if (!Yii::$app->user->isGuest) {
            $user = Yii::$app->user->identity;
        } else {
            $user = new User();
        }

        $travelInsurance = new VazhnoTravel();
        $policy = Policy::findByNumber(\Yii::$app->session->get('travel.policyNumber'));
        return $this->render('travelPayment', [
            'user'  => $user,
            'policy'  => $policy,
            'card'  => new BankCard()
        ]);

    }

    public function actionCalculate()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {

            $model = Factory::createInstance(Yii::$app->request->post('type'));

            $model->attributes = Yii::$app->request->post($model->getBaseClassName());
            $model->quantityInsurant = $_POST['VazhnoTravel']['quantityInsurant'];
            //VarDumper::dump($model->risks);
            echo json_encode($model->calculate());

        } else {
            echo 'Bad request';
        }
    }

    public function actionProcess()
    {
        if (!Yii::$app->user->isGuest) {
            $user = Yii::$app->user->identity;
        } else {
            $user = new User();
        }

        $model  = Factory::createInstance(\Yii::$app->session->get('travel.type'));
        //$liveInsurance = new LiveInsuranceForm();
        $model->calculationId = \Yii::$app->session->get('travel.calculationId');
        $model->country       = \Yii::$app->session->get('travel.country');
        $model->dateBegin     = \Yii::$app->session->get('travel.dateBegin');
        $model->dateEnd       = \Yii::$app->session->get('travel.dateEnd');
        $model->isManyTravel  = \Yii::$app->session->get('travel.isManyTravel');
        $model->quantityInsurant = \Yii::$app->session->get('travel.quantityInsurant');

        if ($user->load(Yii::$app->request->post()) && $model->load(Yii::$app->request->post())  && Model::validateMultiple([$user, $model])){

            //VarDumper::dump($user, 10, true);

            if (!$user->save()) {
                VarDumper::dump($user->errors, 10, true);
            }

            if (Yii::$app->user->isGuest) {
                Yii::$app->user->login($user);
            }

            $outRegisteredPolicy = $model->process($user);

            //VarDumper::dump($outRegisteredPolicy, 10, true);

            if ($outRegisteredPolicy->error === false) {
                //если нет ошибок от Провайдера
                $policy = $model->savePolicyInDB($user, $outRegisteredPolicy);
                \Yii::$app->session->set('travel.policyNumber', $policy->number);
            } else {
                Yii::error(VarDumper::dumpAsString($outRegisteredPolicy, 10, true), 'application');
            }

            echo json_encode([
                'error'     => false,
                'policyNumber' => $policy->number
            ]);
        } else {
            //VarDumper::dump($this->errorsToString(array_merge($user->errors, $model->errors)), 10, true);
            echo json_encode([
                'error'     => true,
                'message'   => 'Возникола ошибка при обработке полиса:'. $this->errorsToString(array_merge($user->errors, $model->errors))
            ]);
            //VarDumper::dump($user->errors, 10, true);
            //VarDumper::dump($liveInsurance->errors, 10, true);
            //$this->redirect([Yii::$app->user->getReturnUrl(), '#' => 'process']);
        }
    }

    public function actionBuy()
    {
        $card = new BankCard();

        if(Yii::$app->request->isAjax && Yii::$app->request->isPost){

//            $r = Yii::$app->request->post('type');
//            $r = Yii::$app->request->post();
//            $model = Factory::createInstance(Yii::$app->request->post('type'));
            $model = Factory::createInstance(\Yii::$app->session->get('travel.type'));
            if($card->load(Yii::$app->request->post()) && $card->validate()){

                $result = $model->buy(\Yii::$app->session->get('travel.policyNumber'), $card);

                //echo VarDumper::dump($result, 10 ,true);

                echo json_encode($result);
            }
        }
    }

    public function actionOrder()
    {
        if(Yii::$app->request->isAjax && Yii::$app->request->isPost){

            $model = Factory::createInstance(\Yii::$app->session->get('travel.type'));
            $response = $model->order(Yii::$app->request->post('orderId'));
            $policy = Policy::findByNumber(\Yii::$app->session->get('travel.policyNumber'));

            if(!$policy){
                echo json_encode(['error' => true, 'message' => 'Не найден номер полиса.']);
            }

            if($response->error){

                $errorMessage = "<b>Ошибка запроса</b>: ".$response->message.".";

                if($response->message == Policy::STATUS_CANCEL){
                    $repeatPayment = ' '.Html::a('Повторить оплату?', ['/cabinet/buy', 'policyNumber'=> $policy->number]);
                    $errorMessage.= $repeatPayment;
                }

                echo json_encode(['error' => true, 'message' => $errorMessage]);
            }else{
                if($policy){

                    //Удаляем старый загруженный полис и сохраняем новый
                    $policyPdf          = $model->pdf($policy->policy_id);
                    if(isset($policyPdf) && !$policyPdf->error){
                        $folder = $model->getFolder();

                        if(Yii::$app->fs->has($policy->url_pdf)){
                            Yii::$app->fs->delete($policy->url_pdf);
                        }
                        Yii::$app->fs->write($folder.$policyPdf->filename, base64_decode($policyPdf->data));
                        $policy->url_pdf = $folder.$policyPdf->filename;
                    }

                    $policy->status     = Policy::STATUS_PAID;
                    $policy->date_paid  = Policy::formatDate($response->date);
                    $policy->transaction= Yii::$app->request->post('orderId');
                    $policy->save(false);

                    $this->sendSuccessEmail($policy);
                }

                $successMessage = 'Полис '.$policy->number.' успешно оплачен. Вы можете скачать полис этой <a href="/files/'.$policy->url_pdf.'">ссылке<a>.';
                echo json_encode(['error' => false, 'message' => $successMessage]);
            }
        }
    }

    protected function sendSuccessEmail($policy)
    {
        Yii::$app->mailer->compose(['html' => 'PaidPolicy-html.php'], ['policy' => $policy])
            ->setFrom(['no-reply@lookins.ru'])
            ->setTo($policy->email)
            ->setSubject('Ваш полис оплачен.')
            ->attach(Yii::getAlias('@webroot/files/').$policy->url_pdf)
            ->send();
    }

    protected function errorsToString(array $errors)
    {
        $message = '';
        if(count($errors) > 0){
            foreach($errors as $attribute){
                foreach($attribute as $k => $v){
                    $message.= $v;
                }
            }
            return $message;
        }
        return '';
    }

}