<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user frontend\models\User */

$link = Yii::$app->urlManager->createAbsoluteUrl(['/files/'.$policy->url_pdf]);
?>
<div>
    <p>Добрый день, <?=$policy->surname.' '.$policy->name;?></p>

    <p>Вами был успешно приобретен полис <?= strtolower($policy->insurance_type);?>.</p>
    <p>Полис находится в приложении к письму, а также его можно скачать по ссылке: <?= Html::a($link, $link);?></p>
</div>
