$(document).ready(function(){
    $('.loader').fadeIn();
    var form = $('#order-insurance-form');

    $.ajax({
        type: 'POST',
        url: form.attr('action'),
        data: form.serialize()
    })
        .done(function(response){
            var json = $.parseJSON(response);
            console.log(form.serialize())
            console.log(form.attr('action'))
            $('.loader').fadeOut();
            if(json.error){
                $('#response').html('<div class="alert alert-danger" role="alert">' + json.message + '</div>');
            }else{
                $('#response').html('<div class="alert alert-success" role="alert">' + json.message + '</div>');
            }
        })
        .fail(function(response){

        });
});