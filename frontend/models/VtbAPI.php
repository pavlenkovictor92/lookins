<?php

namespace frontend\models;
use Yii;
use frontend\models\InsuranceModelAbstract;
use frontend\models\Policy;
use yii\helpers\ArrayHelper;
use yii\db\Expression;
use yii\helpers\Url;

class VtbAPI extends InsuranceModelAbstract
{
    protected $type = '����������� ���������� �� �����';
    protected $company = '���';
    protected $class = __CLASS__;
    protected $code  = 'vtb_vzr';
    protected $folder = 'vtb/vzr/';

    public function calculate()
    {
        $this->token = $this->authorize($this->getToken());
        $response = $this->requestApi($this->url,[
            'method'    => 'calculation',
            'procedure' => 'vzr.traveler',
            'token'     => $this->token,
            'data'      => $this->prepareCalculationRequestData()
        ]);
        //$response = $this->prepareCalculationRequestData();
        return $response;
    }

    public function process($user)
    {
        $this->token = $this->authorize($this->getToken());

        $request = $this->requestApi($this->url, [
            'method'    => 'policy',
            'procedure' => 'vzr.traveler.add',
            'token'     => $this->token,
            'data'      => $this->prepareDataForAddPolicy($user)
        ]);

        if(isset($request) && !$request->error){
            $this->policyId     = $request->policyId;
            $this->policyNumber = $request->policyNumber;
        }

        return $request;
    }

    public function buy($policy, $card)
    {
        $this->token = $this->authorize($this->getToken());

        $response = $this->requestApi($this->url,[
            'method'    => 'service',
            'procedure' => 'pay.alfa',
            'data'      => [
                'action'  => 'card_req',
                'policyName' => $policy,
                'cardNum'   => $card->number,
                'cardHolder'=> $card->owner,
                'cardYear'  => $card->year,
                'cardMonth' => $card->month,
                'cardCvc'   => $card->cvc,
                'back_url'  => Url::toRoute(['cabinet/buy', 'policyNumber' => $policy], true),
            ]
        ]);
        return $response;
    }



}