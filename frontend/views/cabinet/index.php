<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
?>
<h1>Мои полиса</h1>
<?php if(count($policies) > 0):?>
    <table class="table">
        <tr>
            <th>Дата оформления</th>
            <th>Тип</th>
            <th>Номер полиса</th>
            <th>Название страховой</th>
            <th>Статус</th>
            <th>Стоимость</th>
            <th>Дата окончания полиса</th>
            <th>Ссылка на скачивание pdf</th>
        </tr>

    <?php foreach($policies as $i => $policy):?>
        <tr>
            <td><?=$policy->date_at;?></td>
            <td><?=$policy->insurance_type;?></td>
            <td><?=$policy->number;?></td>
            <td><?=$policy->insurance_company;?></td>
            <td>
                <?=$policy->status;?>
                <?php if($policy->status === \frontend\models\Policy::STATUS_WAIT):?>
                    <br>
                    <?= Html::a('Оплатить', ['/cabinet/buy', 'policyNumber' => $policy->number]);?>
                <?php endif;?>
            </td>
            <td><?=($policy->calculation_cost) ? $policy->calculation_cost : '-';?></td>
            <td><?=($policy->date_end) ? $policy->date_end: '-';?></td>
            <td><?= ($policy->url_pdf) ? Html::a('Скачать', '/files/'.$policy->url_pdf, ['target' => '_blank']) : ' - ';?></td>
        </tr>
    <?php endforeach;?>
    </table>
<?php else:?>
    <p><code>Оформленных полисов нет</code></p>
<?php endif;?>
