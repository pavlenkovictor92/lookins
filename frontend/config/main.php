<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'lookins',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'assetManager' => [
            'appendTimestamp' => true,
        ],
        'user' => [
            'identityClass' => 'frontend\models\User',
            'enableAutoLogin' => true,
        ],
        'guzzle'    => [
            'class'     => 'GuzzleHttp\Client'
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
		'urlManager' => [
			'rules' =>
				[
					'/' => 'site/index',
					'<page:(about|partners|how-this-work|information|contacts|kasko|osago)>.html' => 'site/page',
                    '<page:live-insurance>.html'                                                  => 'insurance/live',
                    '<page:travel-insurance>.html'                                                => 'insurance/travel',
                    '<page:travel-insurance-search>.html'                                         => 'insurance/travel-search',
                    '<page:travel-insurance-advanced>.html'                                       => 'insurance/travel-advanced',
                    '<page:travel-insurance-payment>.html'                                        => 'insurance/travel-payment',
                    '<page:login>.html'                                                           => 'site/login',
                    '<page:forget-password>.html'                                                 => 'site/forget-password',
				],
        ],
        'mailer'    => [
            'class'     => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.yandex.ru',
                'username' => 'no-reply@lookins.ru',
                'password' => 'hj4Yh2jnHsk7emJ',
                'port' => '587',
                'encryption' => 'tls',
            ]
        ],
        'fs' => [
            'class' => 'creocoder\flysystem\LocalFilesystem',
            'path' => '@webroot/files',
        ],
    ],
    'params' => $params,
];
