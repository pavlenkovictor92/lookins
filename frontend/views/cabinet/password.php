<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class="row">
    <div class="col-md-12">
        <?= (Yii::$app->session->getFlash('success')) ? '<div class="alert alert-success" role="alert"><b>'.Yii::$app->session->getFlash('success').'</b></div>' : '';?>
        <?= (Yii::$app->session->getFlash('alert')) ? '<div class="alert alert-danger" role="alert"><b>'.Yii::$app->session->getFlash('alert').'</b></div>' : '';?>
    </div>
</div>
<h5 class="page-header"><b>Изменение пароля:</b></h5>
<div class="row">
    <div class="col-md-6">
        <?php $form = ActiveForm::begin([
            'id'    => 'change-password'
        ]);?>
        <?= $form->field($model, 'password')->passwordInput()->label('Новый пароль');?>
        <?= $form->field($model, 'password_repeat')->passwordInput()->label('Повторный пароль');?>
        <div class="pull-right">
            <?= Html::submitButton('Изменить', ['class' => 'btn btn-success']);?>
        </div>
        <?php ActiveForm::end();?>
    </div>
</div>
