<?php
use frontend\models\VazhnoTravel;
use frontend\assets\MainAsset;
use frontend\assets\ValidationJsAsset;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use yii\helpers\Html;
use yii\jui\AutoComplete;
use yii\helpers\Url;

MainAsset::register($this);
ValidationJsAsset::register($this);

$form = ActiveForm::begin([
    'id'    => 'travel-insurance',
    'enableAjaxValidation'  => false,
    'enableClientValidation'=> false,
]);
?>

<div class="look-travel">

    <div class="look-travel-searching" id="calculation">
            <div class="row">
                <h2 class="col-xs-offset-3 col-xs-9"><?=$model->insuranceType;?></h2>

                <div class="col-xs-offset-2 col-xs-10">
                    <div class="look-travel-searching-main">

                        <div class="col-xs-12 first">
                            <h4>Введите данные</h4>
                        </div>

                        <div class="col-xs-12 first">
                            <div class="look-travel-searching-main-description">
                                С помощью нашего калькулятора страхования
                                выезжающих за рубеж вы сможете рассчитать
                                стоимость и купить туристическую страховку
                                онлайн, оплатив полис банковской картой.
                            </div>
                        </div>

                        <div class="col-xs-4 first">
                            <?= $form->field($model, 'country')->widget(AutoComplete::classname(), [
                                'options'       => ['id'    => 'country', 'placeholder' => 'Страна поездки'],
                                'clientOptions' => ['source' => $model->getApiCountries()],
                            ])->label(false); ?>
                        </div>
                        <div class="col-xs-4 date-block">
                            <div class="date-block-fake">
                                <?= Html::input('text', 'dateFake', '', ['id' => 'dateFake', 'placeholder' => 'Когда?'], false); ?>
                            </div>
                            <div class="date-block-begin">
                                <?= $form->field($model, 'dateBegin')->widget(DatePicker::className(), [
                                    'language'  => 'ru',
                                    'dateFormat'=> 'dd.MM.yyyy',
                                    'options'       => ['id' => 'dateBegin'],
                                    'clientOptions' => [
                                        'minDate'       => '+1',
                                        'defaultDate'   => '+1',
                                    ]
                                ])->label(false);?>
                            </div>
                            <div class="date-block-end">
                                <?= $form->field($model, 'dateEnd')->widget(DatePicker::className(), [
                                    'language'  => 'ru',
                                    'dateFormat'=> 'dd.MM.yyyy',
                                    'options'       => ['id' => 'dateEnd'],
                                    'clientOptions' => [
                                        'minDate'       => '+1',
                                        'defaultDate'   => '+1'
                                    ]
                                ]);?>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <?= $form->field($model, 'quantityInsurant')->dropDownList($model->getQuantityInsurant(), [
                                'id'    => 'quantityInsurant'
                            ])->label(false);?>
                        </div>

                        <div class="col-xs-offset-4 col-xs-4 pull">
                            <?= Html::activeInput('checkbox', $model, 'isManyTravel', ['id' => 'isManyTravel'], false); ?>
                            <?= Html::activeLabel($model, 'isManyTravel', ['for' => 'isManyTravel']); ?>
                        </div>

                        <div class="col-xs-4 center">
                            <?= Html::submitButton('Найти страховку', ['class' => 'btn'])?>
<!--                            <a href="--><?//=Url::toRoute(['/travel-insurance-search.html'])?><!--" class="btn btn-default"></a>-->
                        </div>

                        <div class="clearfix"></div>

                    </div>
                </div>

            </div>
</div>

    <div class="look-travel-content">
        <div class="row">
            <div class="col-xs-12">
                <div class="col-xs-3">
                    <img src="<?= Yii::getAlias("@web");?>/img/look-travels-content-1.png" alt="">
                </div>
                <div class="col-xs-9">
                    <span>Выбираете страну путешествия и срок</span>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="col-xs-3">
                    <img src="<?= Yii::getAlias("@web");?>/img/look-travels-content-2.png" alt="">
                </div>
                <div class="col-xs-9">
                    <span>Выбираете предложение от страховой компании</span>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="col-xs-3">
                    <img src="<?= Yii::getAlias("@web");?>/img/look-travels-content-3.png" alt="">
                </div>
                <div class="col-xs-9">
                    <span>Заполняете данные о путешественниках</span>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="col-xs-3">
                    <img src="<?= Yii::getAlias("@web");?>/img/look-travels-content-4.png" alt="">
                </div>
                <div class="col-xs-9">
                    <span>Оплачиваете и получаетете полис на почту</span>
                </div>
            </div>
        </div>
    </div>
</div>

<?php ActiveForm::end() ?>
<?php $this->registerJsFile('@web/js/vazhno-travel-insurance.js', ['depends' => [\yii\web\JqueryAsset::className(), MainAsset::className()]]);?>

