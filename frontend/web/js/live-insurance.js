$(document).ready(function(){
    $('#buyPolicy').focus();

    $('input:checked').attr('onclick', 'return false');
    $('input:checked').attr('onkeydown', 'return false');
    //validation rules
    //$('#live-insurance').validate({
    //    rules: {
    //        "User[surname]":        "required",
    //        "User[name]":           "required",
    //        "User[middle_name]":    "required",
    //        "User[sex]":            "required",
    //        "User[nationality]":    "required",
    //        "User[date_born]":      "required",
    //        "User[document_type]":  "required",
    //        "User[document_serie]": "required",
    //        "User[document_number]":"required",
    //        "User[document_date]":  "required",
    //        "User[document_by]":    "required",
    //        "User[phone]":          "required",
    //        "User[email]":  {
    //            required    : true,
    //            email       : true
    //        },
    //        "User[city]": "required",
    //        "User[street]": "required",
    //        "User[house]": "required"
    //    }
    //});
    //init time functions
    var getCurrentStringDate = function(date, days, months, years){
        var mydate  = date || new Date();
        var inDays  = parseInt(days)|| 0;
        var inMonths= parseInt(months) || 0;
        var inYears = parseInt(years) || 0;


        mydate.setFullYear(mydate.getFullYear() + inYears);
        mydate.setMonth(mydate.getMonth() + inMonths);
        mydate.setDate(mydate.getDate() + inDays);

        return ('0' + mydate.getDate()).slice(-2) + "."
                + ('0' + (mydate.getMonth() + 1)).slice(-2) + "."
                + mydate.getFullYear();
    };

    var timeFromHandler = function(initDate, initDays, initMonths, initYears){
        var stringDate  = getCurrentStringDate(initDate, initDays, initMonths, initYears);
        var months      = $('#liveinsuranceform-timeinsurance').val();
        var diffFromTo  = ((initDays) || 0) -1 ;//date end -1
        $('#timeFrom').val(stringDate);//Now + 1 day
        $('#liveinsuranceform-timeto').val(getCurrentStringDate(initDate, diffFromTo, months));
    };

    var initRequest = function(){
        $('#total').html('Загрузка...');
        function request(){
            $.ajax({
                type: 'POST',
                url: 'insurance/calculate',
                data: $('#live-insurance').serialize()
                })
                .done(function(response){
                    var json = $.parseJSON(response);
                    $('#total').html(json.premium);
                    $("input[id=calculationId]").val(json.calculationId);
                    $("input[id=calculationCost]").val(json.premium);
                })
                .fail(function(response){
                    $('#total').html('Произошла ошибка. Повторите запрос позже.')
                });
        }
        setTimeout(request, 1000);
    };

    //Init element events
    $('#liveinsuranceform-insuranceamountslider-container').on("slidestop", function(event, ui){
        $('#amount').val(ui.value);
        initRequest();
    });

    $('#amount').on('change', function(event){
        $('#slider-container').slider('option', 'value', $(this).val());
        initRequest();
    });

    $('#timeFrom').on('change', function(){
        timeFromHandler($(this).datepicker("getDate"));
        initRequest();
    });

    $('#liveinsuranceform-timeinsurance').on('change', function(event){
        $('#timeFrom').change();
    });

    $(':checkbox').on('click', function(event){
        var n = $('input:checked').length;
        if(n < 1){
            return false;
        }
        initRequest();
    });

    $('#buyPolicy').on('click', function(event){
        event.preventDefault();
        tabs.showTab('process');
    });

    $('#back').on('click', function(event){
        event.preventDefault();
        tabs.showTab('calculation');
    });

    $('#go').on('click', function(event){
        event.preventDefault();
        var form = $('#live-insurance');

        form.validate();

        if($("#coincides").prop("checked")){
           $('#beneficiary_surname').rules('add',{
               required: true
           });
            $('#beneficiary_name').rules('add',{
                required: true
            });
            $('#beneficiary_surname').rules('add',{
                required: true
            });
            $('#beneficiary_middle_name').rules('add',{
                required: true
            });
            $('#beneficiary_date_born').rules('add',{
                required: true
            });
            $('#beneficiary_phone').rules('add',{
                required: true
            });
            $('#beneficiary_email').rules('add',{
                required: true,
                email   : true
            });
            $('#beneficiary_document_serie').rules('add',{
                required: true
            });
            $('#beneficiary_document_number').rules('add',{
                required: true
            });
            $('#beneficiary_document_date').rules('add',{
                required: true
            });
            $('#beneficiary_document_by').rules('add',{
                required: true
            });
            $('#beneficiary_city').rules('add',{
                required: true
            });
            $('#beneficiary_street').rules('add',{
                required: true
            });
            $('#beneficiary_house').rules('add',{
                required: true
            });
        }
        if(form.valid()){
            $('.loader').fadeIn();
            //tabs.showTab('payment');
            //$('.loader').fadeOut();
            $.ajax({
                type: 'POST',
                url: 'insurance/process',
                data: $('#live-insurance').serialize()
            })
                .done(function(response){
                    var json = $.parseJSON(response);
                    if(json.error){
                        $('.loader').fadeOut();
                        alert(json.message);
                    }else{
                        $("input[id=policyNumber]").val(json.policyNumber);
                        tabs.showTab('payment');
                        $('.loader').fadeOut();
                    }
                })
                .fail(function(response){
                    $('.loader').fadeOut();
                    alert('Произошла ошибка при запросе');
                });
        }else{
            return false;
        }
    });

    //Call function to init date values
    timeFromHandler(null, 1, null, null);//init with current date and +1 day
    initRequest(); //init request for calculate for default values
});