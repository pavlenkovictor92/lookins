<?php

/* @var $this yii\web\View */
/* @var $user frontend\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>
Hello <?= $user->name.' '.$user->surname ?>,

Следуйте по ссылке для сброса пароля:

<?= $resetLink ?>
