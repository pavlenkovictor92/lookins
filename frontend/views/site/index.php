<?php
/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = 'Страховой онлайн супермаркет';
$this->registerMetaTag([
	'description' => 'Покупка страхового полиса онлайн. После оформления страховки и онлайн оплаты, Ваш страховой полис придёт к Вам на электронную почту. Возможность приобрести медицинсткую
	туристическую страховку для выезжающих за рубеж, страхование недвижимости, здоровья, от несчастных случаев, транспорта.',

	//'Оформление страховки онлайн выезжающих за рубеж, страхование нежвижимости и от несчастных случаев. После  Онлайн оформление страхового полиса выезжающих за границу. Оформление
	//страховки на недвижимое имущество и онлайн страхование от несчастных случаев.',
	//Группа «АльфаСтрахование» предлагает купить медицинскую туристическую страховку для выезжающих за рубеж онлайн. Мы также осуществляем ВЗР страхование путешественников для Шенгенской визы в Москве и Санкт-Петербурге
	//Предложения для путешественников: страхование туристов от невыезда, страхование багажа, медицинское и имущественное, от несчастных случаев - купить онлайн
	//Покупайте страховой полис как вам удобно: по телефону, на сайте или с помощью онлайн консультанта. Страхование путешественников, недвижимости, здоровья, транспорта.
]);
?>
        <div class="look">
            <ul>
                <li>
                    <img src="img/l1.png" class="look_image" />
                    <div class="text_look text1">
                        <p>Страхование недвижимости</p>
                        <a href="#" class="a_buy">Купить</a>
                    </div>
                </li>
                <li>
                    <img src="img/l2.png" class="look_image" />
                    <div class="text_look">
                        <p>Страхование выезжающих за рубеж</p>
                        <a href="<?=Url::toRoute(['/travel-insurance.html'])?>" class="a_buy">Купить</a>
                    </div>
                </li>
                <li>
                    <img src="img/l3.png" class="look_image" />
                    <div class="text_look">
                        <p>Страхование от несчастных случаев</p>
                        <a href="<?= Url::to(['/live-insurance.html']);?>" class="a_buy">Купить</a>

                    </div>
                </li>
            </ul>
        </div>

<!--    	<div class="type">
        	<ul>
            	<li>
                	<div class="type_block">
                    	<p>ОСАГО</p>
                        <a href="<?=Url::toRoute(['/site/page', 'page'=>'osago'])?>" class="a_buy">Купить</a>
                    </div>
                </li>
                <li>
                	<div class="type_block">
                    	<p>КАСКО</p>
                        <a href="<?=Url::toRoute(['/site/page', 'page'=>'kasko'])?>" class="a_buy">Купить</a>
                    </div>
                </li>
            </ul>
        </div>
-->
        <div class="step">
            <h2>3 шага оформления страховки:</h2>
            <ul>
                <li>
                    <p><em>1</em> Выберите вид страхования</p>
                    <img src="img/step1.png" />
                    <img src="img/line1.png" class="line1" />
                </li>
                <li>
                    <p><em>2</em> Оплатите онлайн</p>
                    <img src="img/step2.png" />
                    <img src="img/line2.png" class="line2" />
                </li>
                <li>
                    <p><em>3</em> Получите Ваш полис на Email</p>
                    <img src="img/step3.png" />
                </li>
            </ul>
        </div>

		<div class="logotip">
            <h2>Наши партнеры</h2>
            <ul>
                <li><a href="<?=Url::toRoute(['/site/page', 'page'=>'partners'])?>"><img src="img/log7.png" /></a></li>
                <li><a href="<?=Url::toRoute(['/site/page', 'page'=>'partners'])?>"><img src="img/log8.png" /></a></li>
                <li><a href="<?=Url::toRoute(['/site/page', 'page'=>'partners'])?>"><img src="img/log1.png" /></a></li>
            </ul>
            <ul>
                <li><a href="<?=Url::toRoute(['/site/page', 'page'=>'partners'])?>"><img src="img/log6.png" /></a></li>
                <li><a href="<?=Url::toRoute(['/site/page', 'page'=>'partners'])?>"><img src="img/log9.png" /></a></li>
                <li><a href="<?=Url::toRoute(['/site/page', 'page'=>'partners'])?>"><img src="img/log10.png" /></a></li>
            </ul>
            <ul>
                <li><a href="<?=Url::toRoute(['/site/page', 'page'=>'partners'])?>"><img src="img/log11.png" /></a></li>
                <li></li>
                <li></li>
            </ul>

        </div>
