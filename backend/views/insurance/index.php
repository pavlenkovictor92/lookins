<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PolicySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Полисы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="policy-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'summary' => 'Показано {page} из {pageCount}',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'user_id',
            'date_at',
            'insurance_type',
            'number',
            'insurance_company',
            'status',
            'calculation_cost',
            'date_end',
            [
                'label' => 'Ссылка на скачинвание pdf',
                'format' => 'raw',
                'value' => function($data){
                    $link = Yii::$app->urlManagerFrontEnd->createUrl('/files/'.$data->url_pdf);
                    return ($data->url_pdf) ? Html::a('Скачать', $link, ['target' => '_blank']) : ' - ';
                }
            ]
            // 'url:url',
            //'calculation_id',
            //'policy_id',
            //'date_paid',
            //'sex',
            // 'surname',
            // 'name',
            // 'middle_name',
            // 'date_born',
            // 'phone',
            // 'email:email',
            // 'document_type',
            // 'document_serie',
            // 'document_number',
            // 'document_date',
            // 'document_by',
            // 'nationality',
            // 'city',
            // 'street',
            // 'house',
            // 'housing',
            // 'apartment',
            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
