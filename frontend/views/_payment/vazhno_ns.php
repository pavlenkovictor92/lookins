<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\assets\MainAsset;
use yii\widgets\ActiveFormAsset;
use frontend\assets\ValidationJsAsset;
use yii\jui\DatePicker;
use yii\web\JsExpression;
ValidationJsAsset::register($this);
?>
<h5 class="page-header">Оплата полиса</h5>
<div class="row">
    <div class="col-md-6">
        <?php if(isset($policy)):?>
            <?php
            $form = ActiveForm::begin([
                'id'    => 'live-insurance',
                //'enableAjaxValidation'  => true,
                'enableClientValidation' => false,
            ]);?>
        <?php endif;?>
        <?= $form->field($card, 'number')->textInput();?>
        <?= $form->field($card, 'owner')->textInput();?>
        <?= $form->field($card, 'date')->widget(DatePicker::className(), [
            'language'  => 'ru',
            'options'   => [
                'id'        => 'bankcard_date'
            ],
            'clientOptions'  => [
                'minDate'       => 0,
                'changeMonth'   => true,
                'changeYear'    => true,
                'dateFormat'    => 'yy/MM',
                'showButtonPanel'=> true,
                'closeText'    => 'Выбрать',
                'onClose'       => new  JsExpression('function(dateText, inst) {
                    var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                    var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                    $(this).val($.datepicker.formatDate("mm/yy", new Date(year, month, 1)));
                }')
            ]

        ]);?>
        <?= $form->field($card, 'cvc')->textInput();?>
        <?php if(isset($policy)):?>
            <?= Html::hiddenInput('type', $policy->class);?>
            <?= Html::hiddenInput('policyNumber', $policy->number);?>
            <?php ActiveForm::end() ?>
        <?php endif;?>
        <div>
            <?php if(!isset($policy)):?>
                <?= Html::a('Назад', ['#'], ['class' => 'btn', 'id' => 'back_process']);?>
            <?php endif;?>
            <?= Html::a('Оплатить', ['#'], ['class' => 'btn', 'id' => 'pay_money']);?>
        </div>
    </div>
</div>
<div id="response"></div>
<?php $this->registerJsFile('@web/js/vazhno_ns.js', ['depends' => [MainAsset::className(), ActiveFormAsset::className()]]);?>
