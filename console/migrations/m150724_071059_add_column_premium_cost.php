<?php

use yii\db\Schema;
use yii\db\Migration;

class m150724_071059_add_column_premium_cost extends Migration
{
    public function up()
    {
        $this->addColumn('policy', 'calculation_cost', Schema::TYPE_DOUBLE);
    }

    public function down()
    {
        $this->dropColumn('policy', 'calculation_cost');
    }
}
