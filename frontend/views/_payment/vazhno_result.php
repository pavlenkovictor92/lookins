<?php
use yii\helpers\Html;
?>
<div class="well well-lg">
    <?= Html::beginForm('/insurance/order', 'POST', [
        'id'    => 'order-insurance-form'
    ]);?>
    <?= Html::hiddenInput('type', $model->class, ['id' => 'type']);?>
    <?= Html::hiddenInput('policyNumber', $policy->number, ['id' => 'policyNumber']);?>
    <?= Html::hiddenInput('orderId', $orderId);?>
    <?= Html::endForm();?>
    <div id="response"></div>
</div>
<?php $this->registerJsFile('@web/js/vazhno_result.js', ['depends' => [\yii\web\JqueryAsset::className()]]);?>
