<?php
use yii\helpers\Html;
use yii\jui\DatePicker;
use frontend\models\User;
use frontend\models\Document;
use frontend\models\Country;

use frontend\models\VazhnoTravel;
use frontend\assets\MainAsset;
use frontend\assets\ValidationJsAsset;
use yii\widgets\ActiveForm;
use yii\jui\AutoComplete;
use yii\helpers\Url;

MainAsset::register($this);
ValidationJsAsset::register($this);

$form = ActiveForm::begin([
    'id'    => 'travel-insurance',
    'enableAjaxValidation'  => false,
    'enableClientValidation'=> false,
]);
?>

<div class="look-travel-parametrs">
    <div class="row">
        <div class="look-travel-parametrs-description">
            <div class="col-xs-12 first"><h2>Параметры вашего страхового полиса</h2></div>
            <div class="col-xs-8 first">
                <ul>
                    <li><span>Страховая компания</span>     <span>ВАЖНО</span></li>
                    <li><span>Территория страхования</span> <span><?= \Yii::$app->session->get('travel.countryName'); ?></span></li>
                    <li><span>Даты поездки</span>           <span>с <?= \Yii::$app->session->get('travel.dateBegin'); ?> по <?= \Yii::$app->session->get('travel.dateEnd'); ?> </span></li>
                    <li><span>Кол-во путешественников</span><span><?= \Yii::$app->session->get('travel.quantityInsurant'); ?></span></li>
                    <li><span>Медицинское страхование</span><span><?= \Yii::$app->session->get('travel.insuranceAmount'); ?> €</span></li>
                </ul>
                <h2>Дополнительные опции страхования</h2>

                <ul class="options">

                </ul>
                <ul class="look-travel-parametrs-description-total">
                    <li><span>стоимость страхового полиса (на <?= \Yii::$app->session->get('travel.quantityInsurant'); ?> человека)</span>     <span><?= \Yii::$app->session->get('travel.calculationAmount'); ?> <del>P</del></span></li>
                </ul>
            </div>
            <div class="col-xs-4 first">
                <div class="support">Обратите внимание: страховка будет действовать указанное Вами количество дней, но в течение более длительного периода. Срок действия страхового полиса автоматически увеличен на 15 дней, чтобы соответствовать Шенгенским правилам, по которым посольства выдают визы сроком минимум на 15 дней больше, чем путешественник запрашивает. Это позволяет менять даты поездки, но не количество дней страхования.</div>
                <button type="submit" class="btn">Помощь оператора</button>
            </div>
            <div class="col-xs-12 first">
                <h2 class="main-title">Заполните данные о путешественниках:</h2>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-8">
            <div class="row">
                <div class="look-travel-parametrs-details">
                    <h3>Страхователь</h3>
                    <div class="col-xs-4 first"><?= $form->field($registrationForm, 'surname')->textInput();?></div>
                    <div class="col-xs-4"><?= $form->field($registrationForm, 'name')->textInput();?></div>
                    <div class="col-xs-4"><?= $form->field($registrationForm, 'middle_name')->textInput();?></div>
                    <div class="col-xs-4 first">
                        <?= $form->field($registrationForm, 'nationality', ['template' => '{label}<div class="customArrow">{input}</div>{error}{hint}'])->dropDownList(Country::getCountries());?>
                    </div>
                    <div class="col-xs-3">
                        <?= $form->field($registrationForm, 'date_born')->widget(DatePicker::className(),[
                            'dateFormat'=> 'dd.MM.yyyy',
                            'options'   => [
                                'id'    => 'date_born'
                            ],
                            'clientOptions'   => [
                                'id'        => 'date_born',
                                'changeMonth'   => true,
                                'changeYear'    => true,
                                'yearRange'     => '-75:-18',
                                'defaultDate'   => '-25y',
                                'maxDate'       => '0'
                            ]
                        ]);?></div>
                    <div class="col-xs-3" ><?= $form->field($registrationForm, 'place_born')->textInput();?></div>
                    <div class="col-xs-2"><?= $form->field($registrationForm, 'sex', ['template' => '{label}<div class="customArrow">{input}</div>{error}{hint}'])->dropDownList(User::$SEX);?></div>
                    <div class="col-xs-4 clear-left first"><?= $form->field($registrationForm, 'phone')->textInput();?></div>
                    <div class="col-xs-8"><?= $form->field($registrationForm, 'email')->textInput();?></div>
                </div>
            </div>
            <div class="row">
                <div class="look-travel-parametrs-details">
                    <h3>Адрес места жительства или регистрации</h3>
                    <div class="col-xs-4 first">
                        <?= $form->field($registrationForm, 'city')->textInput();?>
                    </div>
                    <div class="col-xs-4">
                        <?= $form->field($registrationForm, 'street')->textInput();?>
                    </div>
                    <div class="col-xs-2">
                        <?= $form->field($registrationForm, 'apartment')->textInput();?>
                    </div>
                    <div class="col-xs-1">
                        <?= $form->field($registrationForm, 'housing')->textInput();?>
                    </div>
                    <div class="col-xs-1">
                        <?= $form->field($registrationForm, 'house')->textInput();?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="look-travel-parametrs-details">
                    <h3>Застрахованные</h3>
                    <div id="like_insurant_container">
                        <div class="col-xs-12 first">
                            <?= Html::checkbox('likeInsurant', false, ['id' => 'likeInsurant']);?>
                            <?= Html::label('Совпадает со страхователем', 'likeInsurant');?>
                        </div>
                        <div id="dynamic_insurant"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-4">
            <div class="look-travel-parametrs-details">
                <h3>Документ</h3>
                <div class="col-xs-12 first">
                    <?= $form->field($registrationForm, 'document_type', ['template' => '{label}<div class="customArrow">{input}</div>{error}{hint}'])->dropDownList(Document::getTypes());?>
                </div>
                <div class="col-xs-7 first">
                    <?= $form->field($registrationForm, 'document_serie')->textInput();?>
                </div>
                <div class="col-xs-5">
                    <?= $form->field($registrationForm, 'document_number')->textInput();?>
                </div>
                <div class="col-xs-12 first">
                    <?= $form->field($registrationForm, 'document_by')->textInput();?>
                </div>
                <div class="col-xs-12 first">
                    <?= $form->field($registrationForm, 'document_date')->widget(DatePicker::className(),[
                        'dateFormat'=> 'dd.MM.yyyy',
                        'options'   => [
                            'id'    => 'document_date'
                        ],
                        'clientOptions'   => [
                            'changeMonth'   => true,
                            'changeYear'    => true,
                            'yearRange'     => '-90:+0',
                            'maxDate'       => '0'
                        ]
                    ]);?>
                </div>
                <div class="col-xs-12 first">
                    <?= $form->field($registrationForm, 'inn')->textInput();?>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <?= Html::submitButton('Перейти к оплате', ['class' => 'btn', 'id' => 'go']);?>
        </div>
        <div class="col-xs-12"></div>
    </div>
</div>

<script id="template" type="text/x-handlebars-template">
    {{#each quantity}}
    <div class="insurant">
        <div class="col-xs-3 first">
            <div class="form-group">
                <label class="control-label" for="insurant_surname_{{n}} ">Фамилия</label>
                <input class="latin form-control" type="text" id="insurant_surname_{{n}}" name="VazhnoTravel[surname_{{n}}]" value="">
            </div>
        </div>
        <div class="col-xs-3">
            <div class="form-group">
                <label class="control-label" for="insurant_name_{{n}}">Имя</label>
                <input class="latin form-control" type="text" id="insurant_name_{{n}}" name="VazhnoTravel[name_{{n}}]" value="">
            </div>
        </div>
        <div class="col-xs-4">
            <div class="form-group">
                <label class="control-label" for="insurant_birthdate_{{n}}">Дата рождения</label>
                <input type="text" id="insurant_birthdate_{{n}}" name="VazhnoTravel[birthdate_{{n}}]" value="" class="hasDatepickerDynamic form-control">
            </div>
        </div>
    </div>

    {{/each}}
</script>


<script>

    $(document).ready(function(){

        var options = '<?= \Yii::$app->session->get('travel.options'); ?>';
        var arr = options.split(',');
        if(arr.length != ''){
            console.log(arr)
            if(arr[arr.length - 2] == 'Страхование ГО при занятии спортом'){
                $('<li><span>' + arr[arr.length - 2] + '</span><span>' + arr[arr.length - 1] + '  €</span></li>').appendTo($('.options'));
                arr.splice(arr.length - 2, 2);
            }

            $.each(arr, function (k, v) {
                $('<li><span>' + v + '</span><span>включено</span></li>').appendTo($('.options'));
            });

        }else{
            $('<li><span> --- </span><span></span></li>').appendTo($('.options'));
        };


        var data = {
            quantity: [
                {
                    n: 1
                }
            ]
        }

        data.quantity = [];
        for(var i = 1; i <= <?= \Yii::$app->session->get('travel.quantityInsurant'); ?>; i++){
            data.quantity.push({n: i})
        }
        var template = Handlebars.compile( $('#template').html() );
        $('#dynamic_insurant').append( template(data) );

    })
</script>


<?php $this->registerJsFile('@web/js/vazhno-travel-insurance.js', ['depends' => [\yii\web\JqueryAsset::className(), MainAsset::className()]]);?>
<?php //$this->registerJsFile('@web/js/registration.js', ['depends' => [\yii\web\JqueryAsset::className()]]);?>