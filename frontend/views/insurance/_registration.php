<?php
use yii\helpers\Html;
use yii\jui\DatePicker;
use frontend\models\User;
use frontend\models\Document;
use frontend\models\Country;
?>
<div>
    <div class="row">
        <div class="col-md-6">
            <h5 class="page-header"><b>Страхователь</b></h5>
            <?= $form->field($registrationForm, 'surname')->textInput();?>
            <?= $form->field($registrationForm, 'name')->textInput();?>
            <?= $form->field($registrationForm, 'middle_name')->textInput();?>
            <?= $form->field($registrationForm, 'sex')->dropDownList(User::$SEX);?>
            <?= $form->field($registrationForm, 'nationality')->dropDownList(Country::getCountries());?>
            <?= $form->field($registrationForm, 'date_born')->widget(DatePicker::className(),[
                'dateFormat'=> 'dd.MM.yyyy',
                'options'   => [
                    'id'    => 'date_born'
                ],
                'clientOptions'   => [
                    'id'        => 'date_born',
                    'changeMonth'   => true,
                    'changeYear'    => true,
                    'yearRange'     => '-75:-18',
                    'defaultDate'   => '-25y',
                    'maxDate'       => '0'
                ]
            ]);?>
            <?= $form->field($registrationForm, 'place_born')->textInput();?>
            <?= $form->field($registrationForm, 'phone')->textInput();?>
            <?= $form->field($registrationForm, 'email')->textInput();?>
            <h5 class="page-header"><b>Личные данные застрахованного лица</b></h5>
            <?= $form->field($registrationForm, 'coincides')->checkbox(['id'    => 'coincides']);?>
        </div>
        <div class="col-md-6">
            <h5 class="page-header"><b>Документ</b></h5>
            <?= $form->field($registrationForm, 'document_type')->dropDownList(Document::getTypes());?>
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($registrationForm, 'document_serie')->textInput();?>
                    <?= $form->field($registrationForm, 'document_date')->widget(DatePicker::className(),[
                        'dateFormat'=> 'dd.MM.yyyy',
                        'options'   => [
                            'id'    => 'document_date'
                        ],
                        'clientOptions'   => [
                            'changeMonth'   => true,
                            'changeYear'    => true,
                            'yearRange'     => '-90:+0',
                            'maxDate'       => '0'
                        ]
                    ]);?>
                    <?= $form->field($registrationForm, 'inn')->textInput();?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($registrationForm, 'document_number')->textInput();?>
                    <?= $form->field($registrationForm, 'document_by')->textInput();?>
                </div>
            </div>
            <h5 class="page-header"><b>Адрес места жительства или регистрации</b></h5>
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($registrationForm, 'city')->textInput();?>
                    <?= $form->field($registrationForm, 'house')->textInput();?>
                    <?= $form->field($registrationForm, 'apartment')->textInput();?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($registrationForm, 'street')->textInput();?>
                    <?= $form->field($registrationForm, 'housing')->textInput();?>
                </div>
            </div>
        </div>
    </div>
    <div id="beneficiary" class="row hide">
        <div class="col-md-6">
            <h5 class="page-header"><b>Страхователь</b></h5>
            <?= $form->field($registrationForm, 'beneficiary_surname')->textInput([
                'id'    => 'beneficiary_surname'
            ]);?>
            <?= $form->field($registrationForm, 'beneficiary_name')->textInput([
                'id'    => 'beneficiary_name'
            ]);?>
            <?= $form->field($registrationForm, 'beneficiary_middle_name')->textInput([
                'id'    => 'beneficiary_middle_name'
            ]);?>
            <?= $form->field($registrationForm, 'beneficiary_sex')->dropDownList(User::$SEX, [
                'id'    => 'beneficiary_sex'
            ]);?>
            <?= $form->field($registrationForm, 'beneficiary_nationality')->dropDownList(Country::getCountries(),[
                'id'    => 'beneficiary_nationality'
            ]);?>
            <?= $form->field($registrationForm, 'beneficiary_date_born')->widget(DatePicker::className(),[
                'dateFormat'=> 'dd.MM.yyyy',
                'options'   => [
                    'id'    => 'beneficiary_date_born'
                ],
                'clientOptions'   => [
                    'changeMonth'   => true,
                    'changeYear'    => true,
                    'yearRange'     => '-75:+0',
                    'defaultDate'   => '-25y',
                    'maxDate'       => '0'
                ]
            ]);?>
            <?= $form->field($registrationForm, 'beneficiary_place_born')->textInput([
                'id'    => 'beneficiary_place_born'
            ]);?>
            <?= $form->field($registrationForm, 'beneficiary_phone')->textInput([
                'id'    => 'beneficiary_phone'
            ]);?>
            <?= $form->field($registrationForm, 'beneficiary_email')->textInput([
                'id'    => 'beneficiary_email'
            ]);?>
        </div>
        <div class="col-md-6">
            <h5 class="page-header"><b>Документ</b></h5>
            <?= $form->field($registrationForm, 'beneficiary_document_type')->dropDownList(Document::getTypes(), [
                'id'    =>  'beneficiary_document_type'
            ]);?>
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($registrationForm, 'beneficiary_document_serie')->textInput([
                        'id'    => 'beneficiary_document_serie'
                    ]);?>
                    <?= $form->field($registrationForm, 'beneficiary_document_date')->widget(DatePicker::className(),[
                        'options'   => [
                            'id'    => 'beneficiary_document_date'
                        ],
                        'dateFormat'=> 'dd.MM.yyyy',
                        'clientOptions'   => [
                            'changeMonth'   => true,
                            'changeYear'    => true,
                            'yearRange'     => '-90:+0',
                            'maxDate'       => '0'
                        ]
                    ]);?>
                    <?= $form->field($registrationForm, 'beneficiary_inn')->textInput([
                        'id'    => 'beneficiary_inn'
                    ]);?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($registrationForm, 'beneficiary_document_number')->textInput([
                        'id'    => 'beneficiary_document_number'
                    ]);?>
                    <?= $form->field($registrationForm, 'beneficiary_document_by')->textInput([
                        'id'    => 'beneficiary_document_by'
                    ]);?>
                </div>
            </div>
            <h5 class="page-header"><b>Адрес места жительства или регистрации</b></h5>
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($registrationForm, 'beneficiary_city')->textInput([
                        'id'    => 'beneficiary_city'
                    ]);?>
                    <?= $form->field($registrationForm, 'beneficiary_house')->textInput([
                        'id'    => 'beneficiary_house'
                    ]);?>
                    <?= $form->field($registrationForm, 'beneficiary_apartment')->textInput([
                        'id'    => 'beneficiary_apartment'
                    ]);?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($registrationForm, 'beneficiary_street')->textInput([
                        'id'    => 'beneficiary_street'
                    ]);?>
                    <?= $form->field($registrationForm, 'beneficiary_housing')->textInput([
                        'id'    => 'beneficiary_housing'
                    ]);?>
                </div>
            </div>
        </div>
    </div>
    <div class="pull-right">
        <?= Html::submitButton('Оплатить', ['class' => 'btn', 'id' => 'go']);?>
    </div>
</div>
<?php $this->registerJsFile('@web/js/registration.js', ['depends' => [\yii\web\JqueryAsset::className()]]);?>