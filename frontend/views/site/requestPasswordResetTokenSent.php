<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

?>
<div class="site-reset-password-sent">
    <h3><b>Инструкции по сбросу пароля отправлены Вам на почту.</b></h3>
</div>
