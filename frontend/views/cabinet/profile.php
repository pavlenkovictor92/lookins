<?php
use yii\widgets\ActiveForm;
use frontend\models\User;
use yii\helpers\Html;
use yii\jui\DatePicker;
use frontend\models\Document;
use frontend\models\Country;
?>
<div>
    <?php $form = ActiveForm::begin([]);?>
        <div class="row">
            <div class="col-md-12">
                <?= (Yii::$app->session->getFlash('success')) ? '<div class="alert alert-success" role="alert"><b>'.Yii::$app->session->getFlash('success').'</b></div>' : '';?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h5 class="page-header"><b>Учетные данные:</b></h5>
                <?= $form->field($user, 'surname')->textInput();?>
                <?= $form->field($user, 'name')->textInput();?>
                <?= $form->field($user, 'middle_name')->textInput();?>
                <?= $form->field($user, 'sex')->dropDownList(User::$SEX);?>
                <?= $form->field($user, 'nationality')->dropDownList(Country::getCountries());?>
                <?= $form->field($user, 'date_born')->widget(DatePicker::className(),[
                    'dateFormat'=> 'dd.MM.yyyy',
                    'language'  => 'ru',
                    'clientOptions'   => [
                        'changeMonth'   => true,
                        'changeYear'    => true,
                        'yearRange'     => '-110:+0',
                        'defaultDate'   => '-25y',
                        'maxDate'       => '0'
                    ]
                ]);?>
                <?= $form->field($user, 'place_born')->textInput();?>
                <?= $form->field($user, 'phone')->textInput();?>
                <?= $form->field($user, 'email')->textInput();?>
            </div>
            <div class="col-md-6">
                <h5 class="page-header"><b>Удостоверяющие документы:</b></h5>
                <?= $form->field($user, 'document_type')->dropDownList(Document::getTypes());?>
                <?= $form->field($user, 'document_serie')->textInput();?>
                <?= $form->field($user, 'document_date')->widget(DatePicker::className(),[
                    'dateFormat'=> 'dd.MM.yyyy',
                    'clientOptions'   => [
                        'changeMonth'   => true,
                        'changeYear'    => true,
                        'yearRange'     => '-90:+0',
                        'maxDate'       => '0'
                    ]
                ]);?>
                <?= $form->field($user, 'inn')->textInput();?>
                <?= $form->field($user, 'document_number')->textInput();?>
                <?= $form->field($user, 'document_by')->textInput();?>
                <h5 class="page-header"><b>Адрес места жительства или регистрации:</b></h5>
                <?= $form->field($user, 'city')->textInput();?>
                <?= $form->field($user, 'house')->textInput();?>
                <?= $form->field($user, 'apartment')->textInput();?>
                <?= $form->field($user, 'street')->textInput();?>
                <?= $form->field($user, 'housing')->textInput();?>

            </div>
            <div class="pull-right">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']);?>
            </div>
        </div>
    <?php ActiveForm::end();?>
</div>

