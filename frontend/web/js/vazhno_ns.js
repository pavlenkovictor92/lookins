$(document).ready(function(){
    $('#bankcard_date').focus(function(){
        $(".ui-datepicker-calendar").hide();
    });

    $('#back_process').on('click', function(event){
        event.preventDefault();
        tabs.showTab('process');
    });

    $('#pay_money').on('click', function(event){
        event.preventDefault();

        //validation rules
        var form = $('#live-insurance');
        form.validate();
        $('#bankcard-number').rules('add', {
            required: true,
            minlength: 16,
            maxlength: 16,
            digits: true
        });
        $('#bankcard-owner').rules('add', {
            required: true
        });
        $('#bankcard_date').rules('add', {
            required: true
        });
        $('#bankcard-cvc').rules('add', {
            required: true,
            minlength: 3,
            maxlength: 3,
            digits: true
        });

        if(form.valid()){
            $('.loader').fadeIn();
            $.ajax({
                type: 'POST',
                url: '/insurance/buy',
                data: $('#live-insurance').serialize()
            })
                .done(function(response){
                    //alert(response);
                    var json = $.parseJSON(response);
                    if (json.error){
                        $('.loader').fadeOut();
                        modalBody.html(json.message);
                        modal.modal();
                        return false;
                    }
                    if(json.redirect.error){
                        $('.loader').fadeOut();
                        modalBody.html('Возникла ошибка при оплате полиса');
                        modal.modal();
                    }else{
                        if(json.redirect.method == 'get'){
                            window.location.href = json.redirect.url;
                        }else if(json.redirect.method == 'post'){
                            var html = '';
                            html += '<form id="downloadForm" action="'+json.redirect.url+'" method="POST">';
                            html += '<input type="hidden" name="PaReq" value="'+json.redirect.params.PaReq+'">';
                            html += '<input type="hidden" name="TermUrl" value="'+json.redirect.params.TermUrl+'">';
                            html += '<input type="hidden" name="MD" value="'+json.redirect.params.MD+'">';
                            html +='</form>';

                            $('.footer').append(html);
                            $('.loader').fadeOut();
                            $('#downloadForm').submit();
                        }
                    }
                })
                .fail(function(response){
                    $('.loader').fadeOut();
                    modalBody.html('Возникла ошибка при оплате полиса');
                    modal.modal();
                });
        }else{
            return false;
        }
    });
});
