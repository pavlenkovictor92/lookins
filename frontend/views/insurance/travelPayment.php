<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\assets\MainAsset;
use yii\widgets\ActiveFormAsset;
use frontend\assets\ValidationJsAsset;
use yii\jui\DatePicker;
use yii\web\JsExpression;
ValidationJsAsset::register($this);
?>

<h2 class="look-travel-payment-title">Оплата полиса</h2>
<div class="row">
    <div class="col-md-8">
        <div class="look-travel-payment">
            <?php if(isset($policy)):?>
                <?php
                $form = ActiveForm::begin([
                    'id'    => 'travel-insurance',
                    //'enableAjaxValidation'  => true,
                    'enableClientValidation' => false,
                ]);?>
            <?php endif;?>
            <div class="col-md-12 first">
                <h3 class="pull-left">Введите данные</h3>
                <ul class="pull-right">
                    <li>
                        <img src="/img/visa.png">
                        <img src="/img/mc.png">
                    </li>
                </ul>
            </div>
            <div class="col-md-6 first"><?= $form->field($card, 'number')->textInput();?></div>
            <div class="col-md-6"><?= $form->field($card, 'owner')->textInput();?></div>
            <div class="col-md-3 first">
                <?= $form->field($card, 'date')->widget(DatePicker::className(), [
                    'language'  => 'ru',
                    'options'   => [
                        'id'        => 'bankcard_date'
                    ],
                    'clientOptions'  => [
                        'minDate'       => 0,
                        'changeMonth'   => true,
                        'changeYear'    => true,
                        'dateFormat'    => 'yy/MM',
                        'showButtonPanel'=> true,
                        'closeText'    => 'Выбрать',
                        'onClose'       => new  JsExpression('function(dateText, inst) {
                    var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                    var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                    $(this).val($.datepicker.formatDate("mm/yy", new Date(year, month, 1)));
                }')
                    ]

                ]);?>
            </div>
            <div class="col-md-3"><?= $form->field($card, 'cvc')->textInput();?></div>
            <?php if(isset($policy)):?>
                <?= Html::hiddenInput('type', $policy->class);?>
                <?= Html::hiddenInput('policyNumber', $policy->number);?>
                <?php ActiveForm::end() ?>
            <?php endif;?>
<!--            <input type="text" value="--><?php //=\Yii::$app->session->get('travel.policyNumber');?><!--">-->
<!--            --><?php //= \Yii::$app->session->get('travel.policyNumber'); ?>
            <div class="col-md-12 first">
                <!--            --><?php //if(!isset($policy)):?>
                <!--                --><?php //= Html::a('Назад', ['#'], ['class' => 'btn', 'id' => 'back_process']);?>
                <!--            --><?php //endif;?>
                <?= Html::submitButton('Оплатить', ['class' => 'btn', 'id' => 'pay_money']);?>
            </div>
        </div>
    </div>
</div>
<div id="response"></div>

<?php //$this->registerJsFile('@web/js/vazhno-travel-insurance.js', ['depends' => [\yii\web\JqueryAsset::className(), MainAsset::className()]]);?>
<?php //$this->registerJsFile('@web/js/registration.js', ['depends' => [\yii\web\JqueryAsset::className()]]);?>
<?php $this->registerJsFile('@web/js/vazhno_vzr.js', ['depends' => [MainAsset::className(), ActiveFormAsset::className()]]);?>
