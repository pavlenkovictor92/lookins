<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Policy */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Policies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="policy-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'date_at',
            'insurance_type',
            'number',
            'insurance_company',
            'status',
            'cost',
            'date_end',
            'url:url',
            'calculation_id',
            'policy_id',
            'date_paid',
            'sex',
            'surname',
            'name',
            'middle_name',
            'date_born',
            'phone',
            'email:email',
            'document_type',
            'document_serie',
            'document_number',
            'document_date',
            'document_by',
            'nationality',
            'city',
            'street',
            'house',
            'housing',
            'apartment',
        ],
    ]) ?>

</div>
