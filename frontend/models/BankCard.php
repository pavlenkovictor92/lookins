<?php
/**
 * Created by PhpStorm.
 * User: 6yt9IBka
 * Date: 29.07.2015
 * Time: 15:01
 */

namespace frontend\models;
use yii\base\Model;

class BankCard extends Model
{
    public $number;
    public $owner;
    public $date;
    public $cvc;

    public function getMonth()
    {
        $date_parts = explode('/', $this->date);
        return (int)$date_parts[0];//return month
    }

    public function getYear()
    {
        $date_parts = explode('/', $this->date);
        return (int)$date_parts[1];
    }

    public function rules()
    {
        return [
            [['number', 'cvc'], 'filter',  'filter' => 'trim'],
            [['number', 'owner', 'date', 'cvc'], 'required', 'message' => 'Поле {attribute} обязательно для заполнения'],
            [['number'], 'match', 'pattern' => '/^[0-9]{16}$/', 'message' => 'Номер карты должен состоять из 16 числовых символов'],
            ['cvc', 'match', 'pattern' => '/^[0-9]{3}$/', 'message' => 'CVC-код карты должен состоять из 3 числовых символов']
            //[['cvc'], 'integer', 'message' => 'Поле {attribute} должно быть числовым'],

        ];
    }

    public function attributeLabels()
    {
        return [
            'number'    => 'Номер карты',
            'owner'     => 'Владелец карты',
            'date'     => 'Действует до',
            'cvc'       => 'CVC-код'
        ];
    }
}