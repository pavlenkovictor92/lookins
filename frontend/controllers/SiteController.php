<?php

namespace frontend\controllers;

use frontend\models\PasswordResetRequestForm;
use frontend\models\Policy;
use frontend\models\ResetPasswordForm;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use frontend\models\LoginForm;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            /*'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],*/
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
		$this->layout = 'main-index';
        return $this->render('index');
    }

	/*
	 * Выводит статические страницы сайта
	 * about|quotes|personal-insurance|corporate-insurance|losses|useful|news
	 */
    public function actionPage($page)
    {
        return $this->render($page);
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionForgetPassword()
    {
        $model = new PasswordResetRequestForm();

        if(Yii::$app->request->isPost && $model->load(Yii::$app->request->post()) && $model->validate()){
            $model->sendEmail();
            return $this->render('requestPasswordResetTokenSent');

        }else{
            return $this->render('requestPasswordResetToken', ['model' => $model]);
        }
    }

    public function actionResetPassword($token)
    {
        try{
            $model = new ResetPasswordForm($token);

            if(Yii::$app->request->isPost && $model->load(Yii::$app->request->post()) && $model->validate()){
                if($model->resetPassword()){
                    return $this->render('resetPasswordSuccess');
                }
            }

            return $this->render('resetPassword', ['model' => $model]);
        }catch (\Exception $e){
            return $this->render('error', [
                'name'      => 'Ошибка доступа',
                'message'   => $e->getMessage()
            ]);
        }
    }
}