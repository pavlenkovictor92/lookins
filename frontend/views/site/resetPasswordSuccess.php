<?php
use yii\helpers\Html;
$this->title = 'Новый пароль успешно сохранен';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-reset-password">
    <h1><?= Html::encode($this->title) ?></h1>
    <p><?= Html::a('Перейти на главну страницу', ['/']);?></p>
    <p><?= Html::a('Перейти на форму авторизации', ['/site/login']);?></p>
</div>
