$(document).ready(function(){
    //validation
    $('#travel-insurance').validate({
        rules: {
            "User[surname]": "required",
            "User[name]": "required",
            "User[middle_name]": "required",
            "User[sex]": "required",
            "User[nationality]": "required",
            "User[date_born]": "required",
            "User[document_type]": "required",
            "User[document_serie]": "required",
            "User[document_number]": "required",
            "User[document_date]": "required",
            "User[document_by]": "required",
            "User[phone]": "required",
            "User[email]": {
                required: true,
                email: true
            },
            "User[city]"                : "required",
            "User[street]"              : "required",
            "User[house]"               : "required",
            "VazhnoTravel[country]"     : "required",
            "VazhnoTravel[surname_1]"   : "required",
            "VazhnoTravel[surname_2]"   : "required",
            "VazhnoTravel[surname_3]"   : "required",
            "VazhnoTravel[surname_4]"   : "required",
            "VazhnoTravel[surname_5]"   : "required",
            "VazhnoTravel[name_1]"      : "required",
            "VazhnoTravel[name_2]"      : "required",
            "VazhnoTravel[name_3]"      : "required",
            "VazhnoTravel[name_4]"      : "required",
            "VazhnoTravel[name_5]"      : "required",
            "VazhnoTravel[birthdate_1]" : "required",
            "VazhnoTravel[birthdate_2]" : "required",
            "VazhnoTravel[birthdate_3]" : "required",
            "VazhnoTravel[birthdate_4]" : "required",
            "VazhnoTravel[birthdate_5]" : "required"
        }
    });

    //init
    //callbacks
    $('#go').on('click', function(event){
        event.preventDefault();
        var form = $('#travel-insurance');
        form.validate();
        if(form.valid()){
            $('.loader').fadeIn();
            //tabs.showTab('payment');
            //$('.loader').fadeOut();
            $.ajax({
                type: 'POST',
                url:   window.location.origin + '/insurance/process',
                data: $('#travel-insurance').serialize()
            }).done(function(response){
                    var json = $.parseJSON(response);
                    if(json.error){
                        $('.loader').fadeOut();
                        alert(json.message);
                    }else{
                        $("input[id=policyNumber]").val(json.policyNumber);
                        $('.loader').fadeOut();
                        window.location.href = window.location.origin + '/insurance/travel-payment'
                    }

            }).fail(function(response){
                var json = $.parseJSON(response);
                    $('.loader').fadeOut();
                    alert('Произошла ошибка при запросе');
            })
        }else{
            return false;
        }

    });

    //$('#back_to_calculate').on('click', function(event){
    //    var i = $('.insurant').length;
    //    $('#quantityInsurant').val(i);
    //});

    $('.part_world_country').on('click', function(event){
        event.preventDefault();
        $(this).parent().find('ul').toggleClass('hide');
    });

    $('.selectCountry').on('click', function(event){
        event.preventDefault();
        $('#country').val($(this).text());
        initRequest();
        $(this).parent().parent().toggleClass('hide');
        setCountry($(this).text());
    });

    $.ui.autocomplete.prototype._renderItem = function (ul, item) {
        item.label = item.label.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(this.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>");
        return $("<li></li>")
            .data("item.autocomplete", item)
            .append("<a>" + item.label + "</a>")
            .appendTo(ul);
    };


    $('#country').on('autocompleteselect', function(event, ui){
        var reportname = ui.item.value;
        //appendTo: '#menu-container'
        setCountry(reportname);
    });

    var setCountry = function(country){
        var block =  $('div.part_world:contains("'+country+'")');
        if(!block){
            alert('Выберите страну из справочника');
            return false;
        }
        $('div.part_world').addClass('hide');
        block.removeClass('hide');

        $('.country').text(block.find('.part_world_country').text());
        $('.countryName').val(block.find('.part_world_country').text());
        initRequest();
    };

    $('.part_world:first').removeClass('hide');

    $('#isActiveSport').on('click', function(event){
        if($(this).prop('checked')){
            $(this).val(1)
        }else{
            $(this).val(0)
        }
        $('#typeSport-container').toggleClass('hide');

        $('#typeSport').trigger('change');
        initRequest();
    });


    $('#typeSport').on('change', function(event){
        $(this).parents('.hint').parent().find('.info').remove();
        $('.sport-hint').find('.sport-' + $("option:selected", this).index()).appendTo($(this).parents('.hint').parent());
        initRequest();
    });


    $( "input[name='VazhnoTravel[risks][]']").on('click', function(event){
        var isGoSport = 9;
        if($(this).val() == isGoSport){
            $('#goSportAmount-container').toggleClass('hide');
        }
        initRequest();
    });

    $('.updates').on('change', 'select',function () {
        initRequest();
    });

    $('#dateEnd, #quantityDays, #insuranceAmount, #insuranceAmountCurrency, #assistanceCompany, #goSportAmount, #goSportCurrency').on('change', function(){
        initRequest();
    });

    $('#quantityInsurant').on('change', function(){
        $(this).attr('value', this.value);
        initRequest();
    });

    $('#request').on('click', function(event){
        event.preventDefault();
        initRequest();
    });
    $('#likeInsurant').on('click', function(){
        var state       = $(this).prop('checked');
        var surname     = $('#user-surname').val();
        var name        = $('#user-name').val();
        var birthdate   = $('#date_born').val();
        if(state){
            if(surname){
                $('#insurant_surname_1').val(translit(surname));
            }
            if(name){
                $('#insurant_name_1').val(translit(name));
            }
            if(birthdate){
                $('#insurant_birthdate_1').val(birthdate);
            }
        }else{
            $('#insurant_surname_1').val('');
            $('#insurant_name_1').val('');
            $('#insurant_birthdate_1').val('');
        }
    });

    $(document.body).on('click', '.hasDatepickerDynamic', function(event){
        $(this).datepicker({
            changeMonth : true,
            changeYear  : true,
            yearRange   : '-85:-0',
            defaultDate : '-35y',
            maxDate     :  '0',
            dateFormat  : 'dd.mm.yy'
        });
        $(this).datepicker("show");
    });
    $(document.body).on('keypress', '.latin', function(key){
        if((key.charCode < 97 || key.charCode > 122) && (key.charCode < 65 || key.charCode > 90) && (key.charCode != 45)) return false;
    });

    //functions
    var getCurrentStringDate = function(date, days, months, years){
        var mydate  = date || new Date();
        var inDays  = parseInt(days)|| 0;
        var inMonths= parseInt(months) || 0;
        var inYears = parseInt(years) || 0;


        mydate.setFullYear(mydate.getFullYear() + inYears);
        mydate.setMonth(mydate.getMonth() + inMonths);
        mydate.setDate(mydate.getDate() + inDays);

        return ('0' + mydate.getDate()).slice(-2) + "."
            + ('0' + (mydate.getMonth() + 1)).slice(-2) + "."
            + mydate.getFullYear();
    };

    var initRequest = function(){
        if(!$('input[name=type]').length > 0){
            return;
        }
        $('#total').html('Загрузка ');
        var dateBegin = $('#dateBegin').val();
        //$('#dateBegin').val($('#dateBegin').val().split('-')[0]);

        function request(){
            $.ajax({
                type: 'POST',
                url:  window.location.origin + '/frontend/web/insurance/calculate',
                data: $('#travel-insurance').serialize()
            }).done(function(response){
                    var json = $.parseJSON(response);
                    if(json.error){
                        $('#total').html('---');
                        $('#by').attr('disabled', true);
                        console.log(json)
                    } else {
                        $("input[id=calculationId]").val(json.calculation.calculationId);
                        $("input[id=calculationAmount]").val(json.calculation.premium);
                        $('#total').html(json.calculation.premium + '.00');
                        $('#by').attr('disabled', false);
                    }
            }).fail(function(response){
                    $("input[id=calculationId]").val('');
                    $("input[id=calculationAmount]").val('');
                    $('#total').html('---');
                    $('#by').attr('disabled', true);
            }).always(function(){
                    //$('#dateBegin').val(dateBegin)
            });
        }
        setTimeout(request, 1000);
    };


    var translit = function(text){
        var transl= new Array();
        transl['А']='A';     transl['а']='a';
        transl['Б']='B';     transl['б']='b';
        transl['В']='V';     transl['в']='v';
        transl['Г']='G';     transl['г']='g';
        transl['Д']='D';     transl['д']='d';
        transl['Е']='E';     transl['е']='e';
        transl['Ё']='Yo';    transl['ё']='yo';
        transl['Ж']='Zh';    transl['ж']='zh';
        transl['З']='Z';     transl['з']='z';
        transl['И']='I';     transl['и']='i';
        transl['Й']='I';     transl['й']='i';
        transl['К']='K';     transl['к']='k';
        transl['Л']='L';     transl['л']='l';
        transl['М']='M';     transl['м']='m';
        transl['Н']='N';     transl['н']='n';
        transl['О']='O';     transl['о']='o';
        transl['П']='P';     transl['п']='p';
        transl['Р']='R';     transl['р']='r';
        transl['С']='S';     transl['с']='s';
        transl['Т']='T';     transl['т']='t';
        transl['У']='U';     transl['у']='u';
        transl['Ф']='F';     transl['ф']='f';
        transl['Х']='X';     transl['х']='x';
        transl['Ц']='C';     transl['ц']='c';
        transl['Ч']='Ch';    transl['ч']='ch';
        transl['Ш']='Sh';    transl['ш']='sh';
        transl['Щ']='Shh';    transl['щ']='shh';
        transl['Ъ']='"';     transl['ъ']='"';
        transl['Ы']='Y\'';    transl['ы']='y\'';
        transl['Ь']='\'';    transl['ь']='\'';
        transl['Э']='E\'';    transl['э']='e\'';
        transl['Ю']='Yu';    transl['ю']='yu';
        transl['Я']='Ya';    transl['я']='ya';

        var result='';
        for(i=0;i<text.length;i++) {
            if(transl[text[i]]!=undefined) { result+=transl[text[i]]; }
            else { result+=text[i]; }
        }
        return result;
    };
    var dateBegin = $('#dateBegin');
    var dateEnd = $('#dateEnd');
    var dateFake = $('#dateFake');

    var dateTimeInitiator = function(initDate, initDays, initMonths, initYears){
        var stringDate  = getCurrentStringDate(initDate, initDays, initMonths, initYears);
        dateBegin.val(stringDate);
        var diffDays = initDays || 0;
        var changeDateEnd = getCurrentStringDate(initDate, diffDays + 14, 0, 0);
        dateEnd.val(changeDateEnd);
    };
    dateTimeInitiator(null, 7, null, null);
    initRequest();

    $('.show-advanced-options').on('click', function(e){
        $(this).parents('.look-travel-search-result-block')
               .find('.advanced-options')
               .slideDown('slow');
        $(this).hide()
    });
    $('.hide-advanced-options').on('click', function(e){
        $(this).parents('.look-travel-search-result-block')
               .find('.advanced-options')
               .slideUp('slow');
        $(".show-advanced-options").show();
    });

    //$("#insuranceAmount").on('click', function(){
    //    $(this).parents('label').val($(this).val());
    //    $(this).val()
    //});
    dateBegin.on('change', function(){
        var manyTripsState  = $("#isManyTravel").prop('checked'),
            getDate         = dateBegin.datepicker("getDate");

        if(manyTripsState){
            //Если несколько поездок, то сроки поездко по устанавливается на 1 год
            dateEnd.val(getCurrentStringDate(getDate, -1, 0 , 1));
        }else{
            dateTimeInitiator(getDate);
        }
        initRequest();
    });

    if($("#isManyTravel").prop('checked')){
        $('#quantityDays_parent').toggleClass('hide');
    }

    $('#isManyTravel').on('click', function(event){
        $('#quantityDays_parent').toggleClass('hide');

        var checked = $(this).prop('checked'),
            getDate     = dateBegin.datepicker("getDate"),
            dateTime;

        if(checked){
            $(this).val(1)
            dateTime = getCurrentStringDate(getDate, -1,  0, 1);
            dateEnd.attr('readonly', true);
            dateEnd.datepicker({
                minDate: getDate
            });
        }else{
            $(this).val(0)
            dateTime = getCurrentStringDate(getDate, 14,  0, 0);
            dateEnd.attr('readonly', false);
            dateEnd.datepicker({
                minDate: getDate
            });
        }
        dateEnd.val(dateTime);
        if($('#type').length > 0){
            dateFake.val(dateBegin.val().slice(0, dateBegin.val().indexOf('.', 5)) +  " - " + dateEnd.val().slice(0, dateEnd.val().indexOf('.', 5)));
        }else{
            dateFake.val(dateBegin.val() + " - " + dateEnd.val());
        }
        initRequest();
    });


    dateFake.on('click', function(){
        dateBegin.datepicker('show')
    });

    dateBegin.datepicker({
        beforeShow: function(){
            jQuery( this ).datepicker('option','minDate', getCurrentStringDate(new Date(), 1,  0, 0) );
            jQuery( this ).datepicker('option','maxDate', getCurrentStringDate(new Date(), 0,  0, 1) );
            dateBegin.val('');
            dateEnd.val('');
            dateFake.val('');
            $('#ui-datepicker-div').addClass('main-from').removeClass('main-to');

            //jQuery( this ).datepicker('option','minDate', dateBegin.val() );
        },
        onClose: function( selectedDate ) {
            var date = $(this).datepicker("getDate");

            if(!$('#isManyTravel').prop('checked')){
                dateEnd.datepicker("setDate", date);
                dateEnd.datepicker( "show" );
            }else{
                if($('#type').length > 0){
                    dateFake.val(dateBegin.val().slice(0, dateBegin.val().indexOf('.', 5)) +  " - " + dateEnd.val().slice(0, dateEnd.val().indexOf('.', 5)));
                }else{
                    dateFake.val(dateBegin.val() + " - " + dateEnd.val());
                }
            }
        }
    });

    dateEnd.datepicker({
        //minDate: dateBegin.val(),
        beforeShow: function(){
            $('#ui-datepicker-div').addClass('main-to').removeClass('main-from');
            //$(this).datepicker({
            //    minDate: dateBegin.datepicker("getDate")
            //});
            jQuery( this ).datepicker('option','minDate', getCurrentStringDate(dateBegin.datepicker("getDate"), 1,  0, 0) );
            jQuery( this ).datepicker('option','maxDate', getCurrentStringDate(dateBegin.datepicker("getDate"), 0,  0, 1) );
        },
        //onShow: function(picker, inst) {
        //    $(this).datepicker('option','minDate', dateBegin.val() );
        //var sectionId = $(picker).attr('sectionId');
        //var date1 = "#date1";
        //date1 = date1.concat(sectionId);
        //var dateMin = $(date1).val();
        //picker.datepicker("option", "minDate", dateMin);
        //},
        onClose: function( selectedDate ) {
            var date = $(this).datepicker("getDate");
            var formattedDate = $.datepicker.formatDate('dd.mm.yy', date);
            if($('#type').length > 0){
                dateFake.val(dateBegin.val().slice(0, dateBegin.val().indexOf('.', 5)) +  " - " + formattedDate.slice(0, formattedDate.indexOf('.', 5)));
            }else{
                dateFake.val(dateBegin.val() + " - " + formattedDate);
            }
        }
    });
    var valMapInsurance = [30000, 50000, 100000];

    $('.insuranceAmount').slider({
        max: valMapInsurance.length - 1,
        min: 0,
        slide: function(event, ui) {
            $(".insuranceAmount-result").html(valMapInsurance[ui.value] + " \&euro;");
            $("#insuranceAmount").val(valMapInsurance[ui.value]);
        },
        change: function(event, ui) {
            $("#insuranceAmountMain").val(valMapInsurance[ui.value]);
            initRequest();
            //alert(ui.value);
        }
    });


    var valMapSport = [10000, 30000, 50000];

    $('.goSportAmount').slider({
        max: valMapSport.length - 1,
        min: 0,
        slide: function(event, ui) {
            $(".goSportAmount-result").html(valMapSport[ui.value] + " \&euro;");
            $("#goSportAmount").val(valMapSport[ui.value]);
        },
        change: function(event, ui) {
            $("#goSportAmount").val(valMapSport[ui.value]);
            initRequest();
        }
    });

    //$("#quantityInsurant").on('change', function(){
    //    $(this)
    //})
    $("#quantityInsurant").find('option').each(function( index ) {
        if($(this).val() == 1){
            $(this).html( $(this).text() + " Путешественник" )
        } else if ($(this).val() == 5){
            $(this).html( $(this).text() + " Путешественников")
        } else {
            $(this).html( $(this).text() + " Путешественника")
        }
    });

    $('#typeSport-container').find('.hint').each(function(i,v){
        $(this).parents('label').css('position', 'relative');
        $('.risks-hint').find('.' + $(v).attr('for')).appendTo($(v).parents('label'))
    });

    $('#risks').find('.hint').each(function(i,v){
        $(this).parents('label').css('position', 'relative');
        $('.risks-hint').find('.' + $(v).attr('for')).appendTo($(v).parents('label'))
    });

    $('.hint').mouseover(function() {
        $(this).siblings('.info').css('left', $(this).outerWidth()).show();
    });

    $('.hint').mouseout(function() {
        $(this).siblings('.info').hide();
    });


    $('#typeSport-container').on('mouseover', function() {
        var elemWidth = $(this).outerWidth() + $('label[for = isActiveSport]').outerWidth();
        $(this).siblings('.info').css('left', elemWidth).show();
    });

    $('#typeSport-container').on('mouseout', function() {
        $(this).siblings('.info').hide();
    });

    $('#mainBaggageInsurance').on('click', function(){
        $('#risk-3').click()
        $('#risk-3').prop('checked', $(this).prop('checked') ? true : false)
    });

    $('#mainTripCancel').on('click', function(){
        $('#risk-1').click()
        $('#risk-1').prop('checked', $(this).prop('checked') ? true : false)
    });

    $('#mainCivilResponsibility').on('click', function(){
        $('#risk-9').click()
        $('#risk-9').prop('checked', $(this).prop('checked') ? true : false)
    });

    $('#mainFlightDelay').on('click', function(){
        $('#risk-6').click()
        $('#risk-6').prop('checked', $(this).prop('checked') ? true : false)
    });

    $('#mainAccident').on('click', function(){
        $('#risk-2').click()
        $('#risk-2').prop('checked', $(this).prop('checked') ? true : false)
    });

    $('#mainSport').on('click', function(){
        var that = $(this);
        $('.mainSport').each(function(i, v){
            $('#isActiveSport').trigger('click');
            $(v).siblings('input').prop('checked', that.prop('checked') ? true : false)
       });
    });

    setCountry($('#country').val());

    //$("#quantityInsurant").val($(this).val() + "путешественник");
});