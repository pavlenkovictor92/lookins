<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'Контакты';

?>

<h1><?= Html::encode($this->title) ?></h1>

<p><b>ООО "Капасити"</b></p>
<p>E-mail: <a href="mailto:info@lookins.ru">info@lookins.ru</a></p>
<p>Телефон: +7 (495) 410-10-93</p>