<?php

namespace frontend\controllers;
use frontend\models\BankCard;
use frontend\models\Policy;
use frontend\models\User;
use Yii;
use yii\base\DynamicModel;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\HttpException;

class CabinetController extends Controller
{
    public $layout = 'main';
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'profile', 'change-password', 'buy', 'test'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $user = Yii::$app->user->id;
        $policies = Policy::find()->where('user_id = :user', [':user' => $user])->orderBy('date_at DESC')->all();
        return $this->render('index', [
            'policies' => $policies
        ]);
    }

    public function actionProfile()
    {
        $user = User::findOne(Yii::$app->user->id);

        if($user->load(Yii::$app->request->post()) && $user->validate()){
            $user->save(false);
            Yii::$app->session->setFlash('success', 'Данные успешно обновлены');
        }

        return $this->render('profile', ['user' => $user]);
    }

    public function actionChangePassword()
    {
        $user = User::findOne(Yii::$app->user->id);

        $password = '';
        $password_repeat = '';

        $model = new DynamicModel(compact('password', 'password_repeat'));
        $model->addRule(['password', 'password_repeat'], 'required', ['message' => 'Поле не заполнено'])
            ->addRule('password', 'compare', ['message' => 'Пароли не совпадают'])
            ->addRule(['password', 'password_repeat'], 'string', ['min' => 6, 'message' => 'Пароль должен содержать не менее 6 символов', 'tooShort' => 'Пароль должен содержать не менее 6 символов']);

        if(Yii::$app->request->isPost && $model->load(Yii::$app->request->post()) && $model->validate()){
           $user->setPassword($model->password);
            if($user->save(false)){
                Yii::$app->session->setFlash('success', 'Пароль успешно изменен');
            }else{
                Yii::$app->session->setFlash('alert', VarDumper::dumpAsString($user->errors));
            }
        }
        return $this->render('password', ['model' => $model]);
    }

    public function actionBuy($policyNumber = null, $orderId = null)
    {
        if(is_null($policyNumber)){
            throw new HttpException('Policy not chosen.');
        }

        $policy = Policy::findByNumber($policyNumber);

        if($policy->user_id !== Yii::$app->user->id){
            throw new HttpException('User is not owner of this policy');
        }

        $class = $policy->class;
        $model = new $class;

        if(!is_null($orderId)){
            return $this->render('//_payment/vazhno_result',[
                'model'     => $model,
                'policy'    => $policy,
                'orderId'   => $orderId
            ]);

        }else{

            return $this->render('//_payment/'.$model->code, [
                'policy'    => $policy,
                'card'      => new BankCard()
            ]);
        }
    }
}
