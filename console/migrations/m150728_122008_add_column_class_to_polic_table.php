<?php

use yii\db\Schema;
use yii\db\Migration;

class m150728_122008_add_column_class_to_polic_table extends Migration
{
    public function up()
    {
        $this->addColumn('policy', 'class', Schema::TYPE_STRING.' NOT NULL');
        $this->addColumn('policy', 'transaction', Schema::TYPE_STRING);
    }

    public function down()
    {
        $this->dropColumn('policy', 'transaction');
        $this->dropColumn('policy', 'class');
    }
}
