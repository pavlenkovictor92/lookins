<?php
/**
 * Created by PhpStorm.
 * User: Vuilov
 * Date: 06.07.2015
 * Time: 10:26
 */

namespace frontend\models;
use Yii;
use frontend\models\InsuranceModelAbstract;
use frontend\models\Document;
use frontend\models\Country;
use yii\helpers\VarDumper;
use yii\db\Expression;
use yii\helpers\Url;
class LiveInsuranceForm extends InsuranceModelAbstract
{

    public static $periods = [
        '1' => '1 месяц',
        '2' => '2 месяца',
        '3' => '3 месяца',
        '4' => '4 месяца',
        '5' => '5 месяцев',
        '6' => '6 месяцев',
        '7' => '7 месяцев',
        '8' => '8 месяцев',
        '9' => '9 месяцев',
        '10' => '10 месяцев',
        '11' => '11 месяцев',
        '12' => '1 год',
        '24' => '2 года',
        '36' => '3 года'
    ];
    public static $risks = [
        '0' => 'Смерть в результате несчастного случая (СНС)',
        '1' => 'Инвалидность в результате несчастного случая (ИНС)',
        '2' => 'Телесные повреждения в результате несчастного случая (ТНС)'
    ];
    protected $type = 'Страхование от несчастных случаев';
    protected $company = 'Важно';
    protected $class = __CLASS__;
    protected $code  = 'vazhno_ns';
    protected $folder= 'vazhno/ns/';

    public $insuranceAmount = 100000;
    public $insuranceAmountSlider = 100000;
    public $insuranceRisk = 0;
    public $timeInsurance = 12;
    public $timeFrom;
    public $timeTo;
    public $calculationId;
    public $calculationCost;
    public $policyId;

    public function attributeLabels()
    {
        return [
            'insuranceAmount'   => 'Страховая сумма',
            'insuranceRisk'     => 'Риски',
            'timeInsurance'     => 'Срок страхования'
        ];
    }

    public function rules()
    {
        return [
            [['insuranceAmount', 'insuranceRisk', 'timeInsurance', 'timeFrom', 'timeTo', 'calculationId'], 'required'],
            [['timeFrom', 'timeTo'], 'filter', 'filter' => function($value){
                $date = new \DateTime($value);
                return $date->format('Y.m.d');
            }],
            [['insuranceAmountSlider', 'policyId', 'calculationCost'], 'safe']
        ];
    }

    public function calculate()
    {

        $this->token = $this->authorize($this->getToken());

        $request = $this->requestApi($this->url,[
            'method'    => 'calculation',
            'procedure' => 'ns.general',
            'token'     => $this->token,
            'data'      => [
                'cost'          => $this->insuranceAmount,
                'period'        => $this->timeInsurance,
                'startInsurance'=> $this->timeFrom,
                'risks'         => $this->prepareRisksForApi($this->getNameRisk($this->insuranceRisk))
            ]
        ]);
        return $request->calculation;
    }

    public function process($user)
    {
        $this->token = $this->authorize($this->getToken());

        $request = $this->requestApi($this->url, [
            'method'    => 'policy',
            'procedure' => 'ns.general.add',
            'token'     => $this->token,
            'data'      => $this->prepareDataForAddPolicy($user)
        ]);

        if(isset($request) && !$request->error){
            $this->policyId = $request->policyId;
        }

        return $request;
    }

    public function pdf($policyId)
    {
        $request = $this->requestApi($this->url, [
            'method'    => 'policy',
            'procedure' => 'policy.pdf',
            'token'     => $this->token,
            'data'      => [
                'policyId'  => $policyId
            ]
        ]);

        return $request;
    }

    public function buy($policy, $card)
    {
        $this->token = $this->authorize($this->getToken());

        $response = $this->requestApi($this->url,[
            'method'    => 'service',
            'procedure' => 'pay.alfa',
            'data'      => [
                'action'  => 'card_req',
                'policyName' => $policy,
                'cardNum'   => $card->number,
                'cardHolder'=> $card->owner,
                'cardYear'  => $card->year,
                'cardMonth' => $card->month,
                'cardCvc'   => $card->cvc,
                'back_url'  => Url::toRoute(['cabinet/buy', 'policyNumber' => $policy], true),
            ]
        ]);

        return $response;

    }

    public function order($orderId)
    {
        $this->token = $this->authorize($this->getToken());

        $request    = $this->requestApi($this->url,[
            'method'    => 'service',
            'procedure' => 'pay.alfa',
            'data'      => [
                'action'    => 'card_resp',
                'orderId'   => $orderId
                ]
        ]);

        return $request;
    }

    public function getToken()
    {
        $this->url  = Yii::$app->params['vazhno.me']['url'];
        $request    = $this->requestApi($this->url,[
            'method'    => 'token',
            'procedure' => 'get'
        ]);

        return $request->token;
    }

    protected function authorize($token)
    {
        $request = $this->requestApi($this->url, [
            'method'    => 'authorization',
            'procedure' => 'authorize.b2b',
            'token'     => $token,
            'data'      => [
                'email'     => Yii::$app->params['vazhno.me']['email'],
                'password'  => Yii::$app->params['vazhno.me']['password']
            ]
        ]);
        return $request->token;
    }

    protected function requestApi($url, array $params)
    {
        try{
            $client = Yii::$app->guzzle;
            $response = $client->post($url, [
                'verify'    => false,
                'body'    => $params
            ]);
        }catch (\Exception $e){
            throw $e;
        }
        return json_decode($response->getBody());
    }

    protected function prepareRisksForApi($risks)
    {
        $riskFormat = [];
        $i = 1;
        foreach((array)$risks as $risk){
            $riskFormat['risk'.$i]['risk'] = $risk;
            $i++;
        }
        return $riskFormat;
    }

    protected function prepareDataForAddPolicy($user)
    {
        $preparedData = [
            'calculationId' => $this->calculationId,
            'cost'          => $this->insuranceAmount,
            'period'        => $this->timeInsurance,
            'startInsurance'=> $this->prepareDate($this->timeFrom),
            'stopInsurance' => $this->prepareDate($this->timeTo),
            'insurant'      => [
                'sex'           => $this->getNormalizeGender($user->sex),
                'lastname'      => $user->surname,
                'firstname'     => $user->name,
                'middlename'    => $user->middle_name,
                'birthdate'     => $this->prepareDate($user->date_born),
                'country'       => ($user->place_born) ? $user->place_born : '',
                'phone'         => $user->phone,
                'email'         => $user->email,
                'documentType'  => Document::getTypeByIndex($user->document_type),
                'documentSerie' => $user->document_serie,
                'documentNumber'=> $user->document_number,
                'documentDate'  => $this->prepareDate($user->document_date),
                'documentIssued'=> $user->document_by,
                'inn'           => ($user->inn) ? $user->inn : '',
                'citizenship'   => Country::getCountryByIndex($user->nationality),
                'city'          => $user->city,
                'street'        => $user->street,
                'house'         => $user->house,
                'corpus'        => ($user->housing) ? $user->housing : '',
                'flat'          => ($user->apartment) ? $user->apartment : ''
            ]
        ];

        if($user->coincides > 0){
            $preparedData['insured'] = [
                    'sex'       => $this->getNormalizeGender($user->beneficiary_sex),
                    'lastname'      => $user->beneficiary_surname,
                    'firstname'     => $user->beneficiary_name,
                    'middlename'    => $user->beneficiary_middle_name,
                    'birthdate'     => $this->prepareDate($user->beneficiary_date_born),
                    'country'       => ($user->beneficiary_place_born) ? $user->beneficiary_place_born : '',
                    'phone'         => $user->beneficiary_phone,
                    'email'         => $user->beneficiary_email,
                    'documentType'  => Document::getTypeByIndex($user->beneficiary_document_type),
                    'documentSerie' => $user->beneficiary_document_serie,
                    'documentNumber'=> $user->beneficiary_document_number,
                    'documentDate'  => $this->prepareDate($user->beneficiary_document_date),
                    'documentIssued'=> $user->beneficiary_document_by,
                    'inn'           => ($user->beneficiary_inn) ? $user->beneficiary_inn : '',
                    'citizenship'   => Country::getCountryByIndex($user->beneficiary_nationality),
                    'city'          => $user->beneficiary_city,
                    'street'        => $user->beneficiary_street,
                    'house'         => $user->beneficiary_house,
                    'corpus'        => ($user->beneficiary_housing) ? $user->beneficiary_housing : '',
                    'flat'          => ($user->beneficiary_apartment) ? $user->beneficiary_apartment: '',
            ];
            $preparedData['beneficiary'] =  $preparedData['insured'];
        }else{
            $preparedData['insured'] = $preparedData['insurant'];
            $preparedData['beneficiary'] = $preparedData['insurant'];;
        }

        return $preparedData;
    }

    private function getNormalizeGender($index)
    {
        $gender = [
            0   => 'Муж',
            1   => 'Жен'
        ];
        return $gender[$index];
    }

    public function getNameRisk($indices)
    {
        $riskFormat = [];
        foreach($indices as $index){
            $riskFormat[] = self::$risks[$index];
        }
        return $riskFormat;
    }

    private function prepareDate($date)
    {
        $strDate    = str_replace('.', '-', $date, $count = 3);
        if(strpos($strDate, '-') !== false){
            $parts      = explode('-', $strDate);
            return $parts[2].'.'.$parts[1].'.'.$parts[0];
        }else{
            return $date;
        }
    }

    public function savePolicyInDB($user, $outPolicyData)
    {

        $policyDb                 = new Policy();
        $policyDb->user_id        = $user->id;
        $policyDb->date_at        = new Expression('NOW()');
        $policyDb->insurance_type = $this->getInsuranceType();
        $policyDb->number         = $outPolicyData->policyNumber;
        $policyDb->insurance_company = $this->getInsuranceCompany();
        $policyDb->status         = Policy::STATUS_WAIT;
        $policyDb->cost           = $this->insuranceAmount;
        $policyDb->calculation_cost = $this->calculationCost;
        $policyDb->date_start       = $this->timeFrom;
        $policyDb->date_end       = $this->timeTo;
        $policyDb->calculation_id = $this->calculationId;
        $policyDb->policy_id      = $outPolicyData->policyId;
        $policyDb->class          = $this->class;

        if($user->coincides < 1){
            $policyDb->sex          = $user->sex;
            $policyDb->surname      = $user->surname;
            $policyDb->name         = $user->name;
            $policyDb->middle_name         = $user->middle_name;
            $policyDb->phone         = $user->phone;
            $policyDb->email         = $user->email;
            $policyDb->date_born         = $user->date_born;
            $policyDb->document_type         = $user->document_type;
            $policyDb->document_serie         = $user->document_serie;
            $policyDb->document_number         = $user->document_number;
            $policyDb->document_date         = $user->document_date;
            $policyDb->document_by         = $user->document_by;
            $policyDb->nationality         = $user->nationality;
            $policyDb->city         = $user->city;
            $policyDb->street         = $user->street;
            $policyDb->house         = $user->house;
            $policyDb->housing         = $user->housing;
            $policyDb->apartment         = $user->apartment;
        }else{
            $policyDb->sex          = $user->beneficiary_sex;
            $policyDb->surname      = $user->beneficiary_surname;
            $policyDb->name         = $user->beneficiary_name;
            $policyDb->middle_name         = $user->beneficiary_middle_name;
            $policyDb->phone         = $user->beneficiary_phone;
            $policyDb->email         = $user->beneficiary_email;
            $policyDb->date_born         = $user->beneficiary_date_born;
            $policyDb->document_type         = $user->beneficiary_document_type;
            $policyDb->document_serie         = $user->beneficiary_document_serie;
            $policyDb->document_number         = $user->beneficiary_document_number;
            $policyDb->document_date         = $user->beneficiary_document_date;
            $policyDb->document_by         = $user->beneficiary_document_by;
            $policyDb->nationality         = $user->beneficiary_nationality;
            $policyDb->city         = $user->beneficiary_city;
            $policyDb->street         = $user->beneficiary_street;
            $policyDb->house         = $user->beneficiary_house;
            $policyDb->housing         = $user->beneficiary_housing;
            $policyDb->apartment         = $user->beneficiary_apartment;
        }

        $pdf = $this->pdf($this->policyId);
        if(isset($pdf) && !$pdf->error){
            $filename = $this->folder.$pdf->filename;
            Yii::$app->fs->write($filename, base64_decode($pdf->data));
            $policyDb->url_pdf = $filename;
        }

        if($policyDb->save()){
            return $policyDb;
        }else{
            VarDumper::dump($policyDb, 10, true);
        }
    }
}