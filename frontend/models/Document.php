<?php
/**
 * Created by PhpStorm.
 * User: Vuilov
 * Date: 14.07.2015
 * Time: 11:51
 */

namespace frontend\models;
use yii\base\Model;

class Document extends Model
{
    private static $types = [
        'Паспорт гражданина Российской Федерации',
        'Загранпаспорт',
        'Дипломатический паспорт гражданина РФ',
        'Папорт иностранного гражданина',
        'Свидетельство о рождении'
    ];
    public static function getTypes()
    {
        return self::$types;
    }

    public static function getTypeByIndex($index)
    {
        return self::$types[$index];
    }
}