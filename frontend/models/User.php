<?php
namespace frontend\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

    public static $SEX = ['М','Ж'];
    public $coincides = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'surname', 'name', 'middle_name', 'date_born', 'document_type', 'document_serie', 'document_number', 'document_date', 'document_by', 'city', 'street', 'house', 'phone'], 'required', 'message' => 'Поле "{attribute}" не может быть пустым'],
            [['beneficiary_surname', 'beneficiary_name', 'beneficiary_middle_name', 'beneficiary_date_born', 'beneficiary_place_born','beneficiary_document_type', 'beneficiary_document_serie', 'beneficiary_document_number', 'beneficiary_document_date', 'beneficiary_document_by', 'beneficiary_city', 'beneficiary_street', 'beneficiary_house','beneficiary_phone', 'beneficiary_email'], 'required', 'message'=> 'Поле "{attribute}" не может быть пустым',
                'when'      => function($model){
                    return (int)$this->coincides === 1;
                },
                'whenClient' => 'function(attribute, value){return $("#coincides").prop("checked")}'],
            [['status', 'created_at', 'updated_at', 'sex', 'beneficiary_sex'], 'integer'],
            [['created_at', 'updated_at', 'coincides', 'place_born'], 'safe'],
            [['document_by', 'beneficiary_document_by', 'auth_key', 'password_hash'], 'string'],
            [['auth_key'], 'string', 'max' => 32],
            [['password_hash', 'password_reset_token', 'email', 'surname', 'name', 'middle_name', 'nationality', 'place_born', 'document_type', 'document_serie', 'document_number', 'inn', 'city', 'street', 'house', 'housing', 'apartment', 'phone', /*'beneficiary_surname',*/ 'beneficiary_name', 'beneficiary_middle_name', 'beneficiary_nationality', 'beneficiary_place_born', 'beneficiary_document_type', 'beneficiary_document_serie', 'beneficiary_document_number', 'beneficiary_inn', 'beneficiary_city', 'beneficiary_street', 'beneficiary_house', 'beneficiary_housing', 'beneficiary_apartment', 'beneficiary_phone', 'beneficiary_email'], 'string', 'max' => 255],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            [['date_born', 'document_date', 'beneficiary_date_born', 'beneficiary_document_date'], 'filter', 'filter' => function($value){
                /*$date = \DateTime::createFromFormat('d.m.Y', $value);
                return $date->format('Y-m-d');*/
                $date = strtotime($value);
                return date('Y-m-d', $date);
            }],
            [['email'], 'unique', 'message' => 'Такой {attribute} уже используется'],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'status' => 'Статус',
            'created_at' => 'Создан',
            'updated_at' => 'Изменен',
            'surname' => 'Фамилия',
            'name' => 'Имя',
            'middle_name' => 'Отчество',
            'sex' => 'Пол',
            'nationality' => 'Гражданство',
            'date_born' => 'Дата рождения',
            'place_born' => 'Место рождения',
            'document_type' => 'Тип документа',
            'document_serie' => 'Серия',
            'document_number' => 'Номер',
            'document_date' => 'Дата выдачи',
            'document_by' => 'Кем выдан',
            'inn' => 'ИНН',
            'city' => 'Город',
            'street' => 'Улица',
            'house' => 'Дом',
            'housing' => 'Корпус',
            'apartment' => 'Квартира',
            'phone' => 'Телефон',
            'beneficiary_surname' => 'Фамилия',
            'beneficiary_name' => 'Имя',
            'beneficiary_middle_name' => 'Отчество',
            'beneficiary_sex' => 'Пол',
            'beneficiary_nationality' => 'Гражданство',
            'beneficiary_date_born' => 'Дата рождения',
            'beneficiary_place_born' => 'Место рождения',
            'beneficiary_document_type' => 'Тип документа',
            'beneficiary_document_serie' => 'Серия',
            'beneficiary_document_number' => 'Номер',
            'beneficiary_document_date' => 'Дата выдачи',
            'beneficiary_document_by' => 'Кем выдан',
            'beneficiary_inn' => 'ИНН',
            'beneficiary_city' => 'Город',
            'beneficiary_street' => 'Улица',
            'beneficiary_house' => 'Дом',
            'beneficiary_housing' => 'Корпус',
            'beneficiary_apartment' => 'Квартира',
            'beneficiary_phone' => 'Телефон',
            'beneficiary_email' => 'Email',
            'coincides'  => 'Отличается от страхователя'
        ];
    }


    public function beforeSave($insert)
    {
        if(parent::beforeSave($insert)){
            if($insert){
                $password   = Yii::$app->security->generateRandomString(6);
                $hash       = Yii::$app->security->generatePasswordHash($password);

                Yii::$app->mailer->compose('passwordSet', [
                    'user'      => $this,
                    'password'  => $password,
                    'hash'      => $hash
                ])->setFrom(['no-reply@lookins.ru'])
                    ->setTo($this->email)
                    ->setSubject('Регистрация в системе Lookin.ru')
                    ->send();

                $this->password_hash = $hash;
                $this->generateAuthKey();
            }
            return true;
        }
        return false;
    }

}
