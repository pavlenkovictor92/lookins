<?php
return [
    'adminEmail' => 'k.egorov@lookins.ru',
    'supportEmail' => 'k.egorov@lookins.ru',
    'user.passwordResetTokenExpire' => 3600,
];
