<?php

use yii\db\Schema;
use yii\db\Migration;

class m150723_071515_add_pdf_column extends Migration
{
    public function up()
    {
        $this->addColumn('policy', 'url_pdf', Schema::TYPE_TEXT);
    }

    public function down()
    {
        $this->dropColumn('policy', 'url_pdf');
    }
}
