<?php
namespace frontend\models;

class InsuranceProvider extends \yii\data\ArrayDataProvider
{

    public function init(){

        //Get all all authors with their articles
        $query = Authors::find()->with('articles');

        foreach($query->all() as $author) {

            //Get the last published date if there are articles for the author

            if (count($author->articles)) {
                $lastPublished = $query->max('Published');
            } else {
                $lastPublished = null;
            }

            //Add rows with the Author, # of articles and last publishing date

            $this->allModels[] = [
                'name' => $authors->Name,
                'articles' => count($author->articles),
                'last' => $lastPublished,
            ];
        }
    }
}