<?php

use yii\db\Schema;
use yii\db\Migration;

class m150724_093909_alter_column_date_at_todatetime extends Migration
{
    public function up()
    {
        $this->addColumn('policy', 'date_start', Schema::TYPE_DATE.' AFTER cost');
        $this->alterColumn('policy', 'date_at', Schema::TYPE_DATETIME);
        $this->alterColumn('policy', 'date_paid', Schema::TYPE_DATETIME);
    }

    public function down()
    {
        $this->dropColumn('policy', 'date_start');
        $this->alterColumn('policy', 'date_at', Schema::TYPE_DATE);
        $this->alterColumn('policy', 'date_paid', Schema::TYPE_DATE);
    }
}
