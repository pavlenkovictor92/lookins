<?php
use frontend\models\VazhnoTravel;
use frontend\assets\MainAsset;
use frontend\assets\ValidationJsAsset;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use yii\helpers\Html;
use yii\jui\AutoComplete;
use yii\helpers\Url;
use yii\jui\SliderInput;
use yii\widgets\ListView;

?>

<script id="searchResult" type="text/x-handlebars-template">
    <li>
        <h2>{{name}}</h2>
    </li>
</script>


<div class="look-travel-search-result-block">
    <div class="col-md-8">
        <div class="row">
            <div class="col-md-6">
                <img src="<?= Yii::getAlias('@web') ?>/img/log6.png" alt="">
            </div>
            <div class="col-md-6">
                <div class="look-travel-search-result-block-sum">Страховая сумма <br> <b>30 000 Э</b></div>
            </div>
            <div class="col-md-12">
                <div class="show-advanced-options look-travel-search-result-block-options"><span>Дополнительные опции страхования</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="look-travel-search-result-block-price">
            1600, 36 Р
        </div>
        <div class="btn">Купить страховку</div>
    </div>
    <div class="col-md-12">
        <div class="advanced-options">
            <div class="look-travel-search-result-block-options-advanced">
                <div class="row">
<!--                    <div class="col-md-8">-->
<!--                        --><?php //= $form->field($main, 'insuranceAmount', ['template' => '{label}<div class="customArrow">{input}</div>{error}{hint}'])->dropDownList($main->getInsuranceAmount(), [
//                            'id' => 'insuranceAmount'
//                        ]); ?>
<!--                    </div>-->
<!--                    <div class="col-md-4">-->
<!--                        --><?php //= $form->field($main, 'insuranceAmountCurrency', ['template' => '{label}<div class="customArrow">{input}</div>{error}{hint}'])->dropDownList($main->getGoSportCurrency(), [
//                            'id' => 'insuranceAmountCurrency'
//                        ])->label(''); ?>
<!--                    </div>-->
                </div>
                <!--                            <div>-->
                <!--                                <div class="customArrow">-->
                <!--                                    --><?php //= $form->field($main, 'assistanceCompany')->dropDownList($main->getAssistanceCompany(), [
                //                                        'id'    => 'assistanceCompany'
                //                                    ]);?>
                <!--                                </div>-->
                <!--                            </div>-->
                <div class="col-md-12 first">
<!--                    --><?php //= Html::activeInput('checkbox', $main, 'isActiveSport', ['id' => 'isActiveSport'], false); ?>
<!--                    --><?php //= Html::activeLabel($main, 'isActiveSport', ['for' => 'isActiveSport']); ?>
<!--                    <div id="typeSport-container" class="hide">-->
<!--                        --><?php //= $form->field($main, 'typeSport', ['template' => '{label}<div class="customArrow">{input}</div>{error}{hint}'])->dropDownList($main->getTypeSport(), [
//                            'id' => 'typeSport'
//                        ], false)->label(false); ?>
<!--                    </div>-->
                </div>
                <div class="col-md-12 first">
<!--                    --><?php //= $form->field($main, 'risks')->checkboxList($main->getRisks(true), [
//                        'item' =>
//                            function ($index, $label, $name, $checked, $value) {
//                                return Html::checkbox($name, $checked, [
//                                    'value' => $value,
//                                    'label' => '<label for="' . $label . '">' . $label . '</label>',
//                                    'id' => $label,
//                                ]);
//                            },
//                        'id' => 'risks',
//                        'class' => 'all-risks',
//                        'separator' => '<br>'
//                    ], false)->label(false); ?>
                    <div id="goSportAmount-container" class="row hide">
                        <div class="col-md-8">
<!--                            --><?php //= $form->field($main, 'goSportAmount', ['template' => '{label}<div class="customArrow">{input}</div>{error}{hint}'])->dropDownList($main->getGoSportAmounts(), [
//                                'id' => 'goSportAmount'
//                            ]); ?>
                        </div>
                        <div class="col-md-4">
<!--                            --><?php //= $form->field($main, 'goSportCurrency', ['template' => '{label}<div class="customArrow">{input}</div>{error}{hint}'])->dropDownList($main->getGoSportCurrency(), [
//                                'id' => 'goSportCurrency'
//                            ])->label(''); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 first">
<!--                    --><?php //= $form->field($main, 'insuranceAmountSlider')->widget(SliderInput::className(), [
//                        'clientOptions' => [
//                            'min' => 20000,
//                            'max' => 1000000,
//                            "orientation" => "horizontal",
//                            "range" => "min",
//                            'step' => 3500,
//                            'value' => 100000
//                        ]
//                    ])->label(''); ?>
                </div>
                <div class="col-md-12 first">
<!--                    --><?php //= $form->field($main, 'calculationId')->hiddenInput([
//                        'id' => 'calculationId'
//                    ])->label(false); ?>
<!--                    --><?php //= $form->field($main, 'calculationAmount')->hiddenInput([
//                        'id' => 'calculationAmount'
//                    ])->label(false); ?>
<!--                    --><?php //= Html::hiddenInput('type', $main->className()); ?>
<!--                    --><?php //= Html::hiddenInput('policyNumber', '', ['id' => 'policyNumber']); ?>
                </div>
                <div>
                    <b>Сумма: </b><span id="total"></span>
                </div>
            </div>
            <div class="hide-advanced-options look-travel-search-result-block-options-advanced-hide">

            </div>
        </div>
    </div>
</div>

<!--<div class="look-travel-search-result-block">-->
<!--    <div class="col-md-8">-->
<!--        <div class="row">-->
<!--            <div class="col-md-6">-->
<!--                <img src="--><?php //= Yii::getAlias('@web') ?><!--/img/log9.png" alt="">-->
<!--            </div>-->
<!--            <div class="col-md-6">-->
<!--                <div class="look-travel-search-result-block-sum">Страховая сумма <br> <b>30 000 Э</b></div>-->
<!--            </div>-->

<!--            <div class="col-md-12">-->
<!--                <div class="show-advanced-options look-travel-search-result-block-options">-->
<!--                    <span>Дополнительные опции страхования</span>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--    <div class="col-md-4">-->
<!--        <div class="look-travel-search-result-block-price">-->
<!--            1600, 36 Р-->
<!--        </div>-->
<!--        <div class="btn">Купить страховку</div>-->
<!--    </div>-->
<!--    <div class="col-md-12">-->
<!--        <div class="advanced-options">-->
<!--            <div class="look-travel-search-result-block-options-advanced">-->
<!--                <div class="row">-->
<!--                    <div class="col-md-8">-->
<!--                        --><?php //= $form->field($main, 'insuranceAmount', ['template' => '{label}<div class="customArrow">{input}</div>{error}{hint}'])->dropDownList($main->getInsuranceAmount(), [
//                            'id' => 'insuranceAmount'
//                        ]); ?>
<!--                    </div>-->
<!--                    <div class="col-md-4">-->
<!--                        --><?php //= $form->field($main, 'insuranceAmountCurrency', ['template' => '{label}<div class="customArrow">{input}</div>{error}{hint}'])->dropDownList($main->getGoSportCurrency(), [
//                            'id' => 'insuranceAmountCurrency'
//                        ])->label(''); ?>
<!--                    </div>-->
<!--                </div>-->
<!--                <!--                            <div>-->-->
<!--                <!--                                <div class="customArrow">-->-->
<!--                <!--                                    -->--><?php // //= $form->field($main, 'assistanceCompany')->dropDownList($main->getAssistanceCompany(), [
//                //                                        'id'    => 'assistanceCompany'
//                //                                    ]);?>
<!--                <!--                                </div>-->-->
<!--                <!--                            </div>-->-->
<!--                <div class="col-md-12 first">-->
<!--                    --><?php //= Html::activeInput('checkbox', $main, 'isActiveSport', ['id' => 'isActiveSport'], false); ?>
<!--                    --><?php //= Html::activeLabel($main, 'isActiveSport', ['for' => 'isActiveSport']); ?>
<!--                    <div id="typeSport-container" class="hide">-->
<!--                        --><?php //= $form->field($main, 'typeSport', ['template' => '{label}<div class="customArrow">{input}</div>{error}{hint}'])->dropDownList($main->getTypeSport(), [
//                            'id' => 'typeSport'
//                        ], false)->label(false); ?>
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-md-12 first">-->
<!--                    --><?php //= $form->field($main, 'risks')->checkboxList($main->getRisks(true), [
//                        'item' =>
//                            function ($index, $label, $name, $checked, $value) {
//                                return Html::checkbox($name, $checked, [
//                                    'value' => $value,
//                                    'label' => '<label for="' . $label . '">' . $label . '</label>',
//                                    'id' => $label,
//                                ]);
//                            },
//                        'id' => 'risks',
//                        'class' => 'all-risks',
//                        'separator' => '<br>'
//                    ], false)->label(false); ?>
<!--                    <div id="goSportAmount-container" class="row hide">-->
<!--                        <div class="col-md-8">-->
<!--                            --><?php //= $form->field($main, 'goSportAmount', ['template' => '{label}<div class="customArrow">{input}</div>{error}{hint}'])->dropDownList($main->getGoSportAmounts(), [
//                                'id' => 'goSportAmount'
//                            ]); ?>
<!--                        </div>-->
<!--                        <div class="col-md-4">-->
<!--                            --><?php //= $form->field($main, 'goSportCurrency', ['template' => '{label}<div class="customArrow">{input}</div>{error}{hint}'])->dropDownList($main->getGoSportCurrency(), [
//                                'id' => 'goSportCurrency'
//                            ])->label(''); ?>
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-md-12 first">-->
<!--                    --><?php //= $form->field($main, 'insuranceAmountSlider')->widget(SliderInput::className(), [
//                        'clientOptions' => [
//                            'min' => 20000,
//                            'max' => 1000000,
//                            "orientation" => "horizontal",
//                            "range" => "min",
//                            'step' => 3500,
//                            'value' => 100000
//                        ]
//                    ])->label(''); ?>
<!--                </div>-->
<!--                <div class="col-md-12 first">-->
<!--                    --><?php //= $form->field($main, 'calculationId')->hiddenInput([
//                        'id' => 'calculationId'
//                    ])->label(false); ?>
<!--                    --><?php //= $form->field($main, 'calculationAmount')->hiddenInput([
//                        'id' => 'calculationAmount'
//                    ])->label(false); ?>
<!--                    --><?php //= Html::hiddenInput('type', $main->className()); ?>
<!--                    --><?php //= Html::hiddenInput('policyNumber', '', ['id' => 'policyNumber']); ?>
<!--                </div>-->
<!--                <div>-->
<!--                    <b>Сумма: </b><span id="total"></span>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="hide-advanced-options look-travel-search-result-block-options-advanced-hide">-->

<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->

<!--<div class="look-travel-search-result-block">-->
<!--    <div class="col-md-8">-->
<!--        <div class="row">-->
<!--            <div class="col-md-6">-->
<!--                <img src="--><?php //= Yii::getAlias('@web') ?><!--/img/log9.png" alt="">-->
<!--            </div>-->
<!--            <div class="col-md-6">-->
<!--                <div class="look-travel-search-result-block-sum">Страховая сумма <br> <b>30 000 Э</b></div>-->
<!--            </div>-->

<!--            <div class="col-md-12">-->
<!--                <div class="show-advanced-options look-travel-search-result-block-options">-->
<!--                    <span>Дополнительные опции страхования</span>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--    <div class="col-md-4">-->
<!--        <div class="look-travel-search-result-block-price">-->
<!--            1600, 36 Р-->
<!--        </div>-->
<!--        <div class="btn">Купить страховку</div>-->
<!--    </div>-->
<!--    <div class="col-md-12">-->
<!--        <div class="advanced-options">-->
<!--            <div class="look-travel-search-result-block-options-advanced">-->
<!--                <div class="row">-->
<!--                    <div class="col-md-8">-->
<!--                        --><?php //= $form->field($main, 'insuranceAmount', ['template' => '{label}<div class="customArrow">{input}</div>{error}{hint}'])->dropDownList($main->getInsuranceAmount(), [
//                            'id' => 'insuranceAmount'
//                        ]); ?>
<!--                    </div>-->
<!--                    <div class="col-md-4">-->
<!--                        --><?php //= $form->field($main, 'insuranceAmountCurrency', ['template' => '{label}<div class="customArrow">{input}</div>{error}{hint}'])->dropDownList($main->getGoSportCurrency(), [
//                            'id' => 'insuranceAmountCurrency'
//                        ])->label(''); ?>
<!--                    </div>-->
<!--                </div>-->
<!--                <!--                            <div>-->-->
<!--                <!--                                <div class="customArrow">-->-->
<!--                <!--                                    -->--><?php // //= $form->field($main, 'assistanceCompany')->dropDownList($main->getAssistanceCompany(), [
//                //                                        'id'    => 'assistanceCompany'
//                //                                    ]);?>
<!--                <!--                                </div>-->-->
<!--                <!--                            </div>-->-->
<!--                <div class="col-md-12 first">-->
<!--                    --><?php //= Html::activeInput('checkbox', $main, 'isActiveSport', ['id' => 'isActiveSport'], false); ?>
<!--                    --><?php //= Html::activeLabel($main, 'isActiveSport', ['for' => 'isActiveSport']); ?>
<!--                    <div id="typeSport-container" class="hide">-->
<!--                        --><?php //= $form->field($main, 'typeSport', ['template' => '{label}<div class="customArrow">{input}</div>{error}{hint}'])->dropDownList($main->getTypeSport(), [
//                            'id' => 'typeSport'
//                        ], false)->label(false); ?>
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-md-12 first">-->
<!--                    --><?php //= $form->field($main, 'risks')->checkboxList($main->getRisks(true), [
//                        'item' =>
//                            function ($index, $label, $name, $checked, $value) {
//                                return Html::checkbox($name, $checked, [
//                                    'value' => $value,
//                                    'label' => '<label for="' . $label . '">' . $label . '</label>',
//                                    'id' => $label,
//                                ]);
//                            },
//                        'id' => 'risks',
//                        'class' => 'all-risks',
//                        'separator' => '<br>'
//                    ], false)->label(false); ?>
<!--                    <div id="goSportAmount-container" class="row hide">-->
<!--                        <div class="col-md-8">-->
<!--                            --><?php //= $form->field($main, 'goSportAmount', ['template' => '{label}<div class="customArrow">{input}</div>{error}{hint}'])->dropDownList($main->getGoSportAmounts(), [
//                                'id' => 'goSportAmount'
//                            ]); ?>
<!--                        </div>-->
<!--                        <div class="col-md-4">-->
<!--                            --><?php //= $form->field($main, 'goSportCurrency', ['template' => '{label}<div class="customArrow">{input}</div>{error}{hint}'])->dropDownList($main->getGoSportCurrency(), [
//                                'id' => 'goSportCurrency'
//                            ])->label(''); ?>
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-md-12 first">-->
<!--                    --><?php //= $form->field($main, 'insuranceAmountSlider')->widget(SliderInput::className(), [
//                        'clientOptions' => [
//                            'min' => 20000,
//                            'max' => 1000000,
//                            "orientation" => "horizontal",
//                            "range" => "min",
//                            'step' => 3500,
//                            'value' => 100000
//                        ]
//                    ])->label(''); ?>
<!--                </div>-->
<!--                <div class="col-md-12 first">-->
<!--                    --><?php //= $form->field($main, 'calculationId')->hiddenInput([
//                        'id' => 'calculationId'
//                    ])->label(false); ?>
<!--                    --><?php //= $form->field($main, 'calculationAmount')->hiddenInput([
//                        'id' => 'calculationAmount'
//                    ])->label(false); ?>
<!--                    --><?php //= Html::hiddenInput('type', $main->className()); ?>
<!--                    --><?php //= Html::hiddenInput('policyNumber', '', ['id' => 'policyNumber']); ?>
<!--                </div>-->
<!--                <div>-->
<!--                    <b>Сумма: </b><span id="total"></span>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="hide-advanced-options look-travel-search-result-block-options-advanced-hide">-->
<!---->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
