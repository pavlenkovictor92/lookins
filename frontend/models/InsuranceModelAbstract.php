<?php
/**
 * Created by PhpStorm.
 * User: Vuilov
 * Date: 08.07.2015
 * Time: 12:15
 */

namespace frontend\models;
use yii\base\Model;

abstract class InsuranceModelAbstract extends Model
{
    public $url;
    public $login;
    public $password;
    public $token;
    protected $code = null;
    protected $type;
    protected $company;
    protected $class;
    protected $folder;

    public function getBaseClassName()
    {
        return basename(str_replace('\\', '/', get_class($this)));
    }

    abstract public function calculate();
    abstract public function process($user);
    abstract public function buy($policy, $card);

    public function getInsuranceType()
    {
        return $this->type;
    }

    public function getInsuranceCompany()
    {
        return $this->company;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getClass()
    {
        return $this->class;
    }

    public function getFolder()
    {
        return $this->folder;
    }
}