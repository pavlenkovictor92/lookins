<?php

namespace frontend\models;
use Yii;
use yii\db\Expression;
use frontend\models\User;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "policy".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $date_at
 * @property string $insurance_type
 * @property string $number
 * @property string $insurance_company
 * @property string $status
 * @property string $cost
 * @property string $date_end
 * @property string $url
 * @property string $calculation_id
 * @property string $policy_id
 * @property string $date_paid
 *
 * @property User $user
 */
class Policy extends \yii\db\ActiveRecord
{
    const STATUS_WAIT   = 'Ожидает оплаты';
    const STATUS_PAID   = 'Оплачен';
    const STATUS_CANCEL = 'Платеж отклонен';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'policy';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'date_at', 'number', 'calculation_id'], 'required'],
            [['user_id', 'cost', 'policy_id'], 'integer'],
            [['date_at', 'date_end', 'date_paid'], 'safe'],
            [['insurance_type', 'number', 'insurance_company', 'status', 'url', 'calculation_id'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'date_at' => 'Дата добавления',
            'insurance_type' => 'Тип страхования',
            'number' => 'Номер полиса',
            'insurance_company' => 'Название страховой',
            'status' => 'Статус',
            'cost' => 'Стоимость',
            'date_end' => 'Дата окончания',
            'url' => 'Ссыка на скачивание',
            'calculation_id' => 'ID расчета из API',
            'policy_id' => 'ID полиса из API',
            'date_paid' => 'Дата оплаты',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function findByNumber($number)
    {
        return Policy::find()->where('number = :number', [':number' => $number])->one();
    }

    public static function formatDate($datetime)
    {
        $timestamp = explode(' ', $datetime);
        $date = explode('.', $timestamp[0]);

        return $date[2].'-'.$date[1].'-'.$date[0].' '.$timestamp[1];
    }
}
