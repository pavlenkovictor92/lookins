<?php

use yii\db\Schema;
use yii\db\Migration;

class m150709_113138_create_table_policy extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('policy', [
            'id'                => 'pk',
            'user_id'           => Schema::TYPE_INTEGER.' NOT NULL',
            'date_at'           => Schema::TYPE_DATE.' NOT NULL',
            'insurance_type'    => Schema::TYPE_STRING,
            'number'            => Schema::TYPE_STRING.' NOT NULL',
            'insurance_company' => Schema::TYPE_STRING,
            'status'            => Schema::TYPE_STRING,
            'cost'              => Schema::TYPE_STRING,
            'date_end'          => Schema::TYPE_DATE,
            'url'               => Schema::TYPE_STRING,
            'calculation_id'    => Schema::TYPE_STRING. ' NOT NULL',
            'policy_id'         => Schema::TYPE_INTEGER,
            'date_paid'         => Schema::TYPE_DATE,
            'sex'               => Schema::TYPE_SMALLINT.' NOT NULL',
            'surname'          => Schema::TYPE_STRING.' NOT NULL',
            'name'         => Schema::TYPE_STRING.' NOT NULL',
            'middle_name'        => Schema::TYPE_STRING.' NOT NULL',
            'date_born'         => Schema::TYPE_STRING.' NOT NULL',
            'phone'             => Schema::TYPE_STRING.' NOT NULL',
            'email'             => Schema::TYPE_STRING.' NOT NULL',
            'document_type'      => Schema::TYPE_STRING.' NOT NULL',
            'document_serie'     => Schema::TYPE_STRING.' NOT NULL',
            'document_number'    => Schema::TYPE_STRING.' NOT NULL',
            'document_date'      => Schema::TYPE_STRING.' NOT NULL',
            'document_by'    => Schema::TYPE_STRING.' NOT NULL',
            'nationality'       => Schema::TYPE_SMALLINT.' NOT NULL',
            'city'       => Schema::TYPE_STRING.' NOT NULL',
            'street'       => Schema::TYPE_STRING.' NOT NULL',
            'house'             => Schema::TYPE_STRING.' NOT NULL',
            'housing'            => Schema::TYPE_STRING.' NOT NULL',
            'apartment'              => Schema::TYPE_STRING.' NOT NULL'
        ]);

        $this->addForeignKey('FK_user', 'policy', 'user_id', 'user', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('FK_user', 'policy');
        $this->dropTable('policy');
    }
}
