<?php

use yii\db\Schema;
use yii\db\Migration;

class m150709_103330_alter_user_tabler extends Migration
{
    public function up()
    {
        //������������
        $this->addColumn('user', 'surname', Schema::TYPE_STRING.' NOT NULL');//�������
        $this->addColumn('user', 'name', Schema::TYPE_STRING.' NOT NULL');//���
        $this->addColumn('user', 'middle_name', Schema::TYPE_STRING.' NOT NULL');//��������
        $this->addColumn('user', 'sex', Schema::TYPE_SMALLINT);//���. 0 - �������, 1 - �������
        $this->addColumn('user', 'nationality', Schema::TYPE_STRING);//�����������
        $this->addColumn('user', 'date_born', Schema::TYPE_DATE.' NOT NULL');//���� ��������
        $this->addColumn('user', 'place_born', Schema::TYPE_STRING.' NOT NULL');//����� ��������
        //�������������� ��������
        $this->addColumn('user', 'document_type', Schema::TYPE_INTEGER.' NOT NULL');//��� ���������
        $this->addColumn('user', 'document_serie', Schema::TYPE_STRING.' NOT NULL');//����� ���������
        $this->addColumn('user', 'document_number', Schema::TYPE_STRING.' NOT NULL');//����� ���������
        $this->addColumn('user', 'document_date', Schema::TYPE_DATE.' NOT NULL');//���� ������ ���������
        $this->addColumn('user', 'document_by', Schema::TYPE_TEXT.' NOT NULL');//��� �����
        $this->addColumn('user', 'inn', Schema::TYPE_STRING);//���
        //����� ����� ���������� ��� �����������
        $this->addColumn('user', 'city', Schema::TYPE_STRING.' NOT NULL');//����� ���������
        $this->addColumn('user', 'street', Schema::TYPE_STRING.' NOT NULL');//�����
        $this->addColumn('user', 'house', Schema::TYPE_STRING.' NOT NULL');//���
        $this->addColumn('user', 'housing', Schema::TYPE_STRING);//������
        $this->addColumn('user', 'apartment', Schema::TYPE_STRING);//��������
        //�������������� ����������
        $this->addColumn('user', 'phone', Schema::TYPE_STRING);//�������
        //������ ������ ��������������� ����
        //�������������� ����
        $this->addColumn('user', 'beneficiary_surname', Schema::TYPE_STRING);//�������
        $this->addColumn('user', 'beneficiary_name', Schema::TYPE_STRING);//���
        $this->addColumn('user', 'beneficiary_middle_name', Schema::TYPE_STRING);//��������
        $this->addColumn('user', 'beneficiary_sex', Schema::TYPE_SMALLINT);//���. 0 - �������, 1 - �������
        $this->addColumn('user', 'beneficiary_nationality', Schema::TYPE_STRING);//�����������
        $this->addColumn('user', 'beneficiary_date_born', Schema::TYPE_DATE);//���� ��������
        $this->addColumn('user', 'beneficiary_place_born', Schema::TYPE_STRING);//����� ��������
        //�������������� ��������
        $this->addColumn('user', 'beneficiary_document_type', Schema::TYPE_STRING);//��� ���������
        $this->addColumn('user', 'beneficiary_document_serie', Schema::TYPE_STRING);//����� ���������
        $this->addColumn('user', 'beneficiary_document_number', Schema::TYPE_STRING);//����� ���������
        $this->addColumn('user', 'beneficiary_document_date', Schema::TYPE_DATE);//���� ������ ���������
        $this->addColumn('user', 'beneficiary_document_by', Schema::TYPE_TEXT);//��� �����
        $this->addColumn('user', 'beneficiary_inn', Schema::TYPE_STRING);//���
        //����� ����� ���������� ��� �����������
        $this->addColumn('user', 'beneficiary_city', Schema::TYPE_STRING);//����� ���������
        $this->addColumn('user', 'beneficiary_street', Schema::TYPE_STRING);//�����
        $this->addColumn('user', 'beneficiary_house', Schema::TYPE_STRING);//���
        $this->addColumn('user', 'beneficiary_housing', Schema::TYPE_STRING);//������
        $this->addColumn('user', 'beneficiary_apartment', Schema::TYPE_STRING);//��������
        //�������������� ����������
        $this->addColumn('user', 'beneficiary_phone', Schema::TYPE_STRING);//�������
        $this->addColumn('user', 'beneficiary_email', Schema::TYPE_STRING);//�����
    }

    public function down()
    {
        $this->dropColumn('user', 'surname');
        $this->dropColumn('user', 'name');
        $this->dropColumn('user', 'middle_name');
        $this->dropColumn('user', 'sex');
        $this->dropColumn('user', 'nationality');
        $this->dropColumn('user', 'date_born');
        $this->dropColumn('user', 'place_born');
        $this->dropColumn('user', 'document_type');
        $this->dropColumn('user', 'document_serie');
        $this->dropColumn('user', 'document_number');
        $this->dropColumn('user', 'document_date');
        $this->dropColumn('user', 'document_by');
        $this->dropColumn('user', 'inn');
        $this->dropColumn('user', 'city');
        $this->dropColumn('user', 'street');
        $this->dropColumn('user', 'house');
        $this->dropColumn('user', 'housing');
        $this->dropColumn('user', 'apartment');
        $this->dropColumn('user', 'phone');
        $this->dropColumn('user', 'beneficiary_surname');
        $this->dropColumn('user', 'beneficiary_name');
        $this->dropColumn('user', 'beneficiary_middle_name');
        $this->dropColumn('user', 'beneficiary_sex');
        $this->dropColumn('user', 'beneficiary_nationality');
        $this->dropColumn('user', 'beneficiary_date_born');
        $this->dropColumn('user', 'beneficiary_place_born');
        $this->dropColumn('user', 'beneficiary_document_type');
        $this->dropColumn('user', 'beneficiary_document_serie');
        $this->dropColumn('user', 'beneficiary_document_number');
        $this->dropColumn('user', 'beneficiary_document_date');
        $this->dropColumn('user', 'beneficiary_document_by');
        $this->dropColumn('user', 'beneficiary_city');
        $this->dropColumn('user', 'beneficiary_street');
        $this->dropColumn('user', 'beneficiary_house');
        $this->dropColumn('user', 'beneficiary_housing');
        $this->dropColumn('user', 'beneficiary_apartment');
        $this->dropColumn('user', 'beneficiary_phone');
        $this->dropColumn('user', 'beneficiary_email');
        $this->dropColumn('user', 'beneficiary_inn');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
