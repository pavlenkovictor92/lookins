<?php
use frontend\models\LiveInsuranceForm;
use yii\widgets\ActiveForm;
use yii\jui\SliderInput;
use yii\jui\DatePicker;
use yii\helpers\Html;
use yii\bootstrap\Tabs;
use frontend\assets\MainAsset;
use frontend\assets\ValidationJsAsset;
use yii\bootstrap\Modal;
MainAsset::register($this);
ValidationJsAsset::register($this);
?>
<h2>Страхование от несчатных случаев</h2>
<div>
    <!-- Nav tabs -->
    <?php
    $form = ActiveForm::begin([
        'id'    => 'live-insurance',
        //'enableAjaxValidation'  => true,
        'enableClientValidation' => false,
    ]);
    ?>
    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="calculation">
            <div>

                <div class="row">
                    <span>20 000</span>
                    <?= $form->field($model, 'insuranceAmountSlider')->widget(SliderInput::className(), [
                        'clientOptions' => [
                            'min'   => 20000,
                            'max'   => 1000000,
                            'step'  => 3500,
                            'value'=> 100000]
                    ])->label('');?>
                    <span>1000 000</span>
                </div>
                <div class="row">
                    <?= $form->field($model, 'insuranceAmount')->textInput([
                        'id'    => 'amount'
                    ]);?>
                </div>
                <div class="row">
                    <label>Срок страхования</label>
                    <?= Html::activeDropDownList($model, 'timeInsurance', LiveInsuranceForm::$periods, [
                        'options' => [
                            'id' => 'timeInsurance'
                        ]
                    ]);?>
                    <span>c</span>
                    <?= DatePicker::widget([
                        'id'        => 'timeFrom',
                        'name'      => 'LiveInsuranceForm[timeFrom]',
                        'language'  => 'ru',
                        'dateFormat'=> 'dd.MM.yyyy',
                        'clientOptions' => [
                            'defaultDate'   => '+2',
                            'minDate'       => 0,

                        ]
                    ]);?>
                    <span>по</span>
                    <?= Html::activeTextInput($model, 'timeTo');?>
                </div>
                <div class="row">
                    <?= $form->field($model, 'insuranceRisk')->checkboxList(LiveInsuranceForm::$risks,[
                        'separator' => '<br>'
                    ]);?>
                </div>
                <?= Html::hiddenInput('type', LiveInsuranceForm::className(), ['id' => 'type']);?>

                <?= $form->field($model, 'calculationId')->hiddenInput(['id' => 'calculationId'])->label(false);?>
                <?= $form->field($model, 'calculationCost')->hiddenInput(['id' => 'calculationCost'])->label(false);?>
                <?= Html::hiddenInput('policyNumber','', ['id' => 'policyNumber']);?>
                <div style="float: right">
                    <a id="buyPolicy" href="#process" class="btn btn-default">Купить</a>
                </div>
                <div class="row">
                    Сумма: <span id="total"></span>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="process">
            <?php echo $this->render('_registration', [
                    'registrationForm' => $registrationForm,
                    'form'              => $form
                    ]);?>
            <div>
                <a href="#calculate" id="back" class="btn btn-default">Назад</a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div role="tabpanel" class="tab-pane" id="payment">
            <?php echo $this->render('//_payment/'.$model->code, [
                'form'  => $form,
                'card'  => $card,
                'model' => $model
            ]);?>
        </div>
    </div>
    <?php ActiveForm::end() ?>
</div>
<?php Modal::begin();?>
<?php Modal::end();?>
<?php $this->registerJsFile('@web/js/live-insurance.js', ['depends' => [\yii\web\JqueryAsset::className()]]);?>